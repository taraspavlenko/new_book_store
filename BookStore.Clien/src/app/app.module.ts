import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import {MatTableModule} from '@angular/material/table';
import {NumberPickerModule} from 'ng-number-picker';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { AccountService } from './share/services/account.service';
import { SideMenuComponent } from './side-menu/side-menu.component';
import { BooksComponent } from './books/books.component';
import { AlertifyService } from './share/services/alertify.service';
import { CartComponent } from './cart/cart.component';
import { MatDialogModule } from '@angular/material/dialog';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
   declarations: [
      AppComponent,
      NavMenuComponent,
      LoginComponent,
      HomeComponent,
      SideMenuComponent,
      BooksComponent,
      CartComponent
   ],
   imports: [
      BrowserModule,
      HttpClientModule,
      AppRoutingModule,
      FormsModule,
      ReactiveFormsModule,
      AngularFontAwesomeModule,
      MatDialogModule,
      BrowserAnimationsModule,
      MatTableModule,
      NumberPickerModule
   ],
   providers: [
      AccountService,
      AlertifyService
   ],
   entryComponents: [
      CartComponent
   ],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }

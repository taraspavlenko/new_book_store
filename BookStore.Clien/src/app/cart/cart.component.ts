import { Component, OnInit } from '@angular/core';
import { GetBookView } from '../share/models';
import { CartService } from '../share/services/cart.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  booksFromStorage: GetBookView[] = [];
  displayedColumns: string[] = ['url', 'title', 'authors', 'genre', 'price', 'quantity'];
  totalPrice = 0.0;
  itemsCount = 1;

  constructor(private cartSer: CartService) { }

  ngOnInit() {
    this.getFromStorage();
    this.getTolalPrice();
  }

  getFromStorage() {
    this.booksFromStorage = this.cartSer.getFromStorage();
  }
  getTolalPrice() {
    this.booksFromStorage.forEach((item: GetBookView) => {
      this.totalPrice = this.totalPrice + item.price;
    });
  }
  onChanged(value: any) {

  }
}

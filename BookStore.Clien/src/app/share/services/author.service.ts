/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { CreateAuthorView } from '../models/create-author-view';
import { UpdateAuthorView } from '../models/update-author-view';
@Injectable({
  providedIn: 'root',
})
class AuthorService extends __BaseService {
  static readonly ApiAuthorGetGetPath = '/api/Author/get';
  static readonly ApiAuthorGetByIdGetPath = '/api/Author/get/{id}';
  static readonly ApiAuthorCreatePostPath = '/api/Author/create';
  static readonly ApiAuthorUpdatePostPath = '/api/Author/update';
  static readonly ApiAuthorDeleteByIdGetPath = '/api/Author/delete/{id}';
  static readonly ApiAuthorRestoreByIdGetPath = '/api/Author/restore/{id}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }
  ApiAuthorGetGetResponse(): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/Author/get`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }  ApiAuthorGetGet(): __Observable<null> {
    return this.ApiAuthorGetGetResponse().pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * @param id undefined
   */
  ApiAuthorGetByIdGetResponse(id: string): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/Author/get/${id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * @param id undefined
   */
  ApiAuthorGetByIdGet(id: string): __Observable<null> {
    return this.ApiAuthorGetByIdGetResponse(id).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * @param authorView undefined
   */
  ApiAuthorCreatePostResponse(authorView?: CreateAuthorView): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = authorView;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/api/Author/create`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * @param authorView undefined
   */
  ApiAuthorCreatePost(authorView?: CreateAuthorView): __Observable<null> {
    return this.ApiAuthorCreatePostResponse(authorView).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * @param authorView undefined
   */
  ApiAuthorUpdatePostResponse(authorView?: UpdateAuthorView): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = authorView;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/api/Author/update`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * @param authorView undefined
   */
  ApiAuthorUpdatePost(authorView?: UpdateAuthorView): __Observable<null> {
    return this.ApiAuthorUpdatePostResponse(authorView).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * @param id undefined
   */
  ApiAuthorDeleteByIdGetResponse(id: string): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/Author/delete/${id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * @param id undefined
   */
  ApiAuthorDeleteByIdGet(id: string): __Observable<null> {
    return this.ApiAuthorDeleteByIdGetResponse(id).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * @param id undefined
   */
  ApiAuthorRestoreByIdGetResponse(id: string): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/Author/restore/${id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * @param id undefined
   */
  ApiAuthorRestoreByIdGet(id: string): __Observable<null> {
    return this.ApiAuthorRestoreByIdGetResponse(id).pipe(
      __map(_r => _r.body as null)
    );
  }
}

module AuthorService {
}

export { AuthorService }

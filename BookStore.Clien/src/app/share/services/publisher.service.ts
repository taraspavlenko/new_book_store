/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { CreatePublisherView } from '../models/create-publisher-view';
import { UpdatePublisherView } from '../models/update-publisher-view';
@Injectable({
  providedIn: 'root',
})
class PublisherService extends __BaseService {
  static readonly ApiPublisherGetGetPath = '/api/Publisher/get';
  static readonly ApiPublisherGetByIdGetPath = '/api/Publisher/get/{id}';
  static readonly ApiPublisherCreatePostPath = '/api/Publisher/create';
  static readonly ApiPublisherUpdatePostPath = '/api/Publisher/update';
  static readonly ApiPublisherDeleteByIdGetPath = '/api/Publisher/delete/{id}';
  static readonly ApiPublisherRestoreByIdGetPath = '/api/Publisher/restore/{id}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }
  ApiPublisherGetGetResponse(): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/Publisher/get`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }  ApiPublisherGetGet(): __Observable<null> {
    return this.ApiPublisherGetGetResponse().pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * @param id undefined
   */
  ApiPublisherGetByIdGetResponse(id: string): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/Publisher/get/${id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * @param id undefined
   */
  ApiPublisherGetByIdGet(id: string): __Observable<null> {
    return this.ApiPublisherGetByIdGetResponse(id).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * @param publisherView undefined
   */
  ApiPublisherCreatePostResponse(publisherView?: CreatePublisherView): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = publisherView;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/api/Publisher/create`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * @param publisherView undefined
   */
  ApiPublisherCreatePost(publisherView?: CreatePublisherView): __Observable<null> {
    return this.ApiPublisherCreatePostResponse(publisherView).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * @param publisherView undefined
   */
  ApiPublisherUpdatePostResponse(publisherView?: UpdatePublisherView): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = publisherView;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/api/Publisher/update`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * @param publisherView undefined
   */
  ApiPublisherUpdatePost(publisherView?: UpdatePublisherView): __Observable<null> {
    return this.ApiPublisherUpdatePostResponse(publisherView).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * @param id undefined
   */
  ApiPublisherDeleteByIdGetResponse(id: string): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/Publisher/delete/${id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * @param id undefined
   */
  ApiPublisherDeleteByIdGet(id: string): __Observable<null> {
    return this.ApiPublisherDeleteByIdGetResponse(id).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * @param id undefined
   */
  ApiPublisherRestoreByIdGetResponse(id: string): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/Publisher/restore/${id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * @param id undefined
   */
  ApiPublisherRestoreByIdGet(id: string): __Observable<null> {
    return this.ApiPublisherRestoreByIdGetResponse(id).pipe(
      __map(_r => _r.body as null)
    );
  }
}

module PublisherService {
}

export { PublisherService }

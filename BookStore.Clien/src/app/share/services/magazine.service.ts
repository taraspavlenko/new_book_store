/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { CreateMagazineView } from '../models/create-magazine-view';
import { UpdateMagazineView } from '../models/update-magazine-view';
@Injectable({
  providedIn: 'root',
})
class MagazineService extends __BaseService {
  static readonly ApiMagazineGetByIdGetPath = '/api/Magazine/get/{id}';
  static readonly ApiMagazineGetGetPath = '/api/Magazine/get';
  static readonly ApiMagazineCreatePostPath = '/api/Magazine/create';
  static readonly ApiMagazineUpdateGetPath = '/api/Magazine/update';
  static readonly ApiMagazineDeleteByIdGetPath = '/api/Magazine/delete/{id}';
  static readonly ApiMagazineRestoreByIdGetPath = '/api/Magazine/restore/{id}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * @param id undefined
   */
  ApiMagazineGetByIdGetResponse(id: string): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/Magazine/get/${id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * @param id undefined
   */
  ApiMagazineGetByIdGet(id: string): __Observable<null> {
    return this.ApiMagazineGetByIdGetResponse(id).pipe(
      __map(_r => _r.body as null)
    );
  }
  ApiMagazineGetGetResponse(): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/Magazine/get`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }  ApiMagazineGetGet(): __Observable<null> {
    return this.ApiMagazineGetGetResponse().pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * @param createMagazineView undefined
   */
  ApiMagazineCreatePostResponse(createMagazineView?: CreateMagazineView): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = createMagazineView;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/api/Magazine/create`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * @param createMagazineView undefined
   */
  ApiMagazineCreatePost(createMagazineView?: CreateMagazineView): __Observable<null> {
    return this.ApiMagazineCreatePostResponse(createMagazineView).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * @param updateMagazineView undefined
   */
  ApiMagazineUpdateGetResponse(updateMagazineView?: UpdateMagazineView): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = updateMagazineView;
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/Magazine/update`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * @param updateMagazineView undefined
   */
  ApiMagazineUpdateGet(updateMagazineView?: UpdateMagazineView): __Observable<null> {
    return this.ApiMagazineUpdateGetResponse(updateMagazineView).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * @param id undefined
   */
  ApiMagazineDeleteByIdGetResponse(id: string): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/Magazine/delete/${id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * @param id undefined
   */
  ApiMagazineDeleteByIdGet(id: string): __Observable<null> {
    return this.ApiMagazineDeleteByIdGetResponse(id).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * @param id undefined
   */
  ApiMagazineRestoreByIdGetResponse(id: string): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/Magazine/restore/${id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * @param id undefined
   */
  ApiMagazineRestoreByIdGet(id: string): __Observable<null> {
    return this.ApiMagazineRestoreByIdGetResponse(id).pipe(
      __map(_r => _r.body as null)
    );
  }
}

module MagazineService {
}

export { MagazineService }

/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { CreateRoleView } from '../models/create-role-view';
import { UpdateRoleView } from '../models/update-role-view';
@Injectable({
  providedIn: 'root',
})
class RoleService extends __BaseService {
  static readonly ApiRoleGetGetPath = '/api/Role/get';
  static readonly ApiRoleGetByIdGetPath = '/api/Role/get/{id}';
  static readonly ApiRoleCreatePostPath = '/api/Role/create';
  static readonly ApiRoleUpdatePostPath = '/api/Role/update';
  static readonly ApiRoleDeleteByIdGetPath = '/api/Role/delete/{id}';
  static readonly ApiRoleRestoreByIdGetPath = '/api/Role/restore/{id}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }
  ApiRoleGetGetResponse(): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/Role/get`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }  ApiRoleGetGet(): __Observable<null> {
    return this.ApiRoleGetGetResponse().pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * @param id undefined
   */
  ApiRoleGetByIdGetResponse(id: string): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/Role/get/${id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * @param id undefined
   */
  ApiRoleGetByIdGet(id: string): __Observable<null> {
    return this.ApiRoleGetByIdGetResponse(id).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * @param createRoleView undefined
   */
  ApiRoleCreatePostResponse(createRoleView?: CreateRoleView): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = createRoleView;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/api/Role/create`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * @param createRoleView undefined
   */
  ApiRoleCreatePost(createRoleView?: CreateRoleView): __Observable<null> {
    return this.ApiRoleCreatePostResponse(createRoleView).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * @param updateRoleView undefined
   */
  ApiRoleUpdatePostResponse(updateRoleView?: UpdateRoleView): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = updateRoleView;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/api/Role/update`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * @param updateRoleView undefined
   */
  ApiRoleUpdatePost(updateRoleView?: UpdateRoleView): __Observable<null> {
    return this.ApiRoleUpdatePostResponse(updateRoleView).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * @param params The `RoleService.ApiRoleDeleteByIdGetParams` containing the following parameters:
   *
   * - `roleId`:
   *
   * - `id`:
   */
  ApiRoleDeleteByIdGetResponse(params: RoleService.ApiRoleDeleteByIdGetParams): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;


    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/Role/delete/${params.id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * @param params The `RoleService.ApiRoleDeleteByIdGetParams` containing the following parameters:
   *
   * - `roleId`:
   *
   * - `id`:
   */
  ApiRoleDeleteByIdGet(params: RoleService.ApiRoleDeleteByIdGetParams): __Observable<null> {
    return this.ApiRoleDeleteByIdGetResponse(params).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * @param params The `RoleService.ApiRoleRestoreByIdGetParams` containing the following parameters:
   *
   * - `roleId`:
   *
   * - `id`:
   */
  ApiRoleRestoreByIdGetResponse(params: RoleService.ApiRoleRestoreByIdGetParams): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;


    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/Role/restore/${params.id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * @param params The `RoleService.ApiRoleRestoreByIdGetParams` containing the following parameters:
   *
   * - `roleId`:
   *
   * - `id`:
   */
  ApiRoleRestoreByIdGet(params: RoleService.ApiRoleRestoreByIdGetParams): __Observable<null> {
    return this.ApiRoleRestoreByIdGetResponse(params).pipe(
      __map(_r => _r.body as null)
    );
  }
}

module RoleService {

  /**
   * Parameters for ApiRoleDeleteByIdGet
   */
  export interface ApiRoleDeleteByIdGetParams {
    roleId: string;
    id: string;
  }

  /**
   * Parameters for ApiRoleRestoreByIdGet
   */
  export interface ApiRoleRestoreByIdGetParams {
    roleId: string;
    id: string;
  }
}

export { RoleService }

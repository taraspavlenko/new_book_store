/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { RegisterUserView } from '../models/register-user-view';
import { LoginView } from '../models/login-view';
import { TokenView } from '../models/token-view';
@Injectable({
  providedIn: 'root',
})
class AccountService extends __BaseService {
  static readonly ApiAccountRegisterPostPath = '/api/Account/register';
  static readonly ApiAccountLoginPostPath = '/api/Account/login';
  static readonly ApiAccountRefreshPostPath = '/api/Account/refresh';
  static readonly ApiAccountConfirmEmailGetPath = '/api/Account/confirmEmail';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * @param registerUserView undefined
   */
  ApiAccountRegisterPostResponse(registerUserView?: RegisterUserView): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = registerUserView;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/api/Account/register`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * @param registerUserView undefined
   */
  ApiAccountRegisterPost(registerUserView?: RegisterUserView): __Observable<null> {
    return this.ApiAccountRegisterPostResponse(registerUserView).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * @param loginView undefined
   */
  ApiAccountLoginPostResponse(loginView?: LoginView): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = loginView;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/api/Account/login`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * @param loginView undefined
   */
  ApiAccountLoginPost(loginView?: LoginView): __Observable<null> {
    return this.ApiAccountLoginPostResponse(loginView).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * @param tokenView undefined
   */
  ApiAccountRefreshPostResponse(tokenView?: TokenView): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = tokenView;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/api/Account/refresh`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * @param tokenView undefined
   */
  ApiAccountRefreshPost(tokenView?: TokenView): __Observable<null> {
    return this.ApiAccountRefreshPostResponse(tokenView).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * @param params The `AccountService.ApiAccountConfirmEmailGetParams` containing the following parameters:
   *
   * - `userId`:
   *
   * - `token`:
   */
  ApiAccountConfirmEmailGetResponse(params: AccountService.ApiAccountConfirmEmailGetParams): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;


    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/Account/confirmEmail`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * @param params The `AccountService.ApiAccountConfirmEmailGetParams` containing the following parameters:
   *
   * - `userId`:
   *
   * - `token`:
   */
  ApiAccountConfirmEmailGet(params: AccountService.ApiAccountConfirmEmailGetParams): __Observable<null> {
    return this.ApiAccountConfirmEmailGetResponse(params).pipe(
      __map(_r => _r.body as null)
    );
  }
}

module AccountService {

  /**
   * Parameters for ApiAccountConfirmEmailGet
   */
  export interface ApiAccountConfirmEmailGetParams {
    userId?: string;
    token?: string;
  }
}

export { AccountService }

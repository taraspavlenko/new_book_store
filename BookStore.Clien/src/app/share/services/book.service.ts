/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { GetBookView } from '../models/get-book-view';
import { GetBooksView } from '../models/get-books-view';
import { CreateBookView } from '../models/create-book-view';
import { UpdateBookView } from '../models/update-book-view';
@Injectable({
  providedIn: 'root',
})
class BookService extends __BaseService {
  static readonly ApiBookGetByIdGetPath = '/api/Book/get/{id}';
  static readonly ApiBookGetGetPath = '/api/Book/get';
  static readonly ApiBookCreatePostPath = '/api/Book/create';
  static readonly ApiBookUpdatePostPath = '/api/Book/update';
  static readonly ApiBookDeleteByIdGetPath = '/api/Book/delete/{id}';
  static readonly ApiBookRestoreByIdGetPath = '/api/Book/restore/{id}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }

  /**
   * @param id undefined
   * @return Success
   */
  ApiBookGetByIdGetResponse(id: string): __Observable<__StrictHttpResponse<GetBookView>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/Book/get/${id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<GetBookView>;
      })
    );
  }
  /**
   * @param id undefined
   * @return Success
   */
  ApiBookGetByIdGet(id: string): __Observable<GetBookView> {
    return this.ApiBookGetByIdGetResponse(id).pipe(
      __map(_r => _r.body as GetBookView)
    );
  }

  /**
   * @return Success
   */
  ApiBookGetGetResponse(): __Observable<__StrictHttpResponse<GetBooksView>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/Book/get`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<GetBooksView>;
      })
    );
  }
  /**
   * @return Success
   */
  ApiBookGetGet(): __Observable<GetBooksView> {
    return this.ApiBookGetGetResponse().pipe(
      __map(_r => _r.body as GetBooksView)
    );
  }

  /**
   * @param createBookView undefined
   */
  ApiBookCreatePostResponse(createBookView?: CreateBookView): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = createBookView;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/api/Book/create`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * @param createBookView undefined
   */
  ApiBookCreatePost(createBookView?: CreateBookView): __Observable<null> {
    return this.ApiBookCreatePostResponse(createBookView).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * @param updateBookView undefined
   */
  ApiBookUpdatePostResponse(updateBookView?: UpdateBookView): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = updateBookView;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/api/Book/update`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * @param updateBookView undefined
   */
  ApiBookUpdatePost(updateBookView?: UpdateBookView): __Observable<null> {
    return this.ApiBookUpdatePostResponse(updateBookView).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * @param id undefined
   */
  ApiBookDeleteByIdGetResponse(id: string): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/Book/delete/${id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * @param id undefined
   */
  ApiBookDeleteByIdGet(id: string): __Observable<null> {
    return this.ApiBookDeleteByIdGetResponse(id).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * @param id undefined
   */
  ApiBookRestoreByIdGetResponse(id: string): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/Book/restore/${id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * @param id undefined
   */
  ApiBookRestoreByIdGet(id: string): __Observable<null> {
    return this.ApiBookRestoreByIdGetResponse(id).pipe(
      __map(_r => _r.body as null)
    );
  }
}

module BookService {
}

export { BookService }

/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { CreateGenreView } from '../models/create-genre-view';
import { UpdateGenreView } from '../models/update-genre-view';
@Injectable({
  providedIn: 'root',
})
class GenreService extends __BaseService {
  static readonly ApiGenreGetGetPath = '/api/Genre/get';
  static readonly ApiGenreGetByIdGetPath = '/api/Genre/get/{id}';
  static readonly ApiGenreCreatePostPath = '/api/Genre/create';
  static readonly ApiGenreUpdatePostPath = '/api/Genre/update';
  static readonly ApiGenreDeleteByIdGetPath = '/api/Genre/delete/{id}';
  static readonly ApiGenreRestoreByIdGetPath = '/api/Genre/restore/{id}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }
  ApiGenreGetGetResponse(): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/Genre/get`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }  ApiGenreGetGet(): __Observable<null> {
    return this.ApiGenreGetGetResponse().pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * @param id undefined
   */
  ApiGenreGetByIdGetResponse(id: string): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/Genre/get/${id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * @param id undefined
   */
  ApiGenreGetByIdGet(id: string): __Observable<null> {
    return this.ApiGenreGetByIdGetResponse(id).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * @param genreView undefined
   */
  ApiGenreCreatePostResponse(genreView?: CreateGenreView): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = genreView;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/api/Genre/create`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * @param genreView undefined
   */
  ApiGenreCreatePost(genreView?: CreateGenreView): __Observable<null> {
    return this.ApiGenreCreatePostResponse(genreView).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * @param genreView undefined
   */
  ApiGenreUpdatePostResponse(genreView?: UpdateGenreView): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = genreView;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/api/Genre/update`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * @param genreView undefined
   */
  ApiGenreUpdatePost(genreView?: UpdateGenreView): __Observable<null> {
    return this.ApiGenreUpdatePostResponse(genreView).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * @param id undefined
   */
  ApiGenreDeleteByIdGetResponse(id: string): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/Genre/delete/${id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * @param id undefined
   */
  ApiGenreDeleteByIdGet(id: string): __Observable<null> {
    return this.ApiGenreDeleteByIdGetResponse(id).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * @param id undefined
   */
  ApiGenreRestoreByIdGetResponse(id: string): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/Genre/restore/${id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * @param id undefined
   */
  ApiGenreRestoreByIdGet(id: string): __Observable<null> {
    return this.ApiGenreRestoreByIdGetResponse(id).pipe(
      __map(_r => _r.body as null)
    );
  }
}

module GenreService {
}

export { GenreService }

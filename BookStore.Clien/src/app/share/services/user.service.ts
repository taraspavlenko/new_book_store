/* tslint:disable */
import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { BaseService as __BaseService } from '../base-service';
import { ApiConfiguration as __Configuration } from '../api-configuration';
import { StrictHttpResponse as __StrictHttpResponse } from '../strict-http-response';
import { Observable as __Observable } from 'rxjs';
import { map as __map, filter as __filter } from 'rxjs/operators';

import { RegisterUserView } from '../models/register-user-view';
import { UpdateUserView } from '../models/update-user-view';
@Injectable({
  providedIn: 'root',
})
class UserService extends __BaseService {
  static readonly ApiUserGetGetPath = '/api/User/get';
  static readonly ApiUserGetByIdGetPath = '/api/User/get/{id}';
  static readonly ApiUserCreatePostPath = '/api/User/create';
  static readonly ApiUserUpdatePostPath = '/api/User/update';
  static readonly ApiUserDeleteByIdGetPath = '/api/User/delete/{id}';
  static readonly ApiUserRestoreByIdGetPath = '/api/User/restore/{id}';

  constructor(
    config: __Configuration,
    http: HttpClient
  ) {
    super(config, http);
  }
  ApiUserGetGetResponse(): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/User/get`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }  ApiUserGetGet(): __Observable<null> {
    return this.ApiUserGetGetResponse().pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * @param id undefined
   */
  ApiUserGetByIdGetResponse(id: string): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/User/get/${id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * @param id undefined
   */
  ApiUserGetByIdGet(id: string): __Observable<null> {
    return this.ApiUserGetByIdGetResponse(id).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * @param registerUserView undefined
   */
  ApiUserCreatePostResponse(registerUserView?: RegisterUserView): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = registerUserView;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/api/User/create`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * @param registerUserView undefined
   */
  ApiUserCreatePost(registerUserView?: RegisterUserView): __Observable<null> {
    return this.ApiUserCreatePostResponse(registerUserView).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * @param updateUserView undefined
   */
  ApiUserUpdatePostResponse(updateUserView?: UpdateUserView): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;
    __body = updateUserView;
    let req = new HttpRequest<any>(
      'POST',
      this.rootUrl + `/api/User/update`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * @param updateUserView undefined
   */
  ApiUserUpdatePost(updateUserView?: UpdateUserView): __Observable<null> {
    return this.ApiUserUpdatePostResponse(updateUserView).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * @param id undefined
   */
  ApiUserDeleteByIdGetResponse(id: string): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/User/delete/${id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * @param id undefined
   */
  ApiUserDeleteByIdGet(id: string): __Observable<null> {
    return this.ApiUserDeleteByIdGetResponse(id).pipe(
      __map(_r => _r.body as null)
    );
  }

  /**
   * @param id undefined
   */
  ApiUserRestoreByIdGetResponse(id: string): __Observable<__StrictHttpResponse<null>> {
    let __params = this.newParams();
    let __headers = new HttpHeaders();
    let __body: any = null;

    let req = new HttpRequest<any>(
      'GET',
      this.rootUrl + `/api/User/restore/${id}`,
      __body,
      {
        headers: __headers,
        params: __params,
        responseType: 'json'
      });

    return this.http.request<any>(req).pipe(
      __filter(_r => _r instanceof HttpResponse),
      __map((_r) => {
        return _r as __StrictHttpResponse<null>;
      })
    );
  }
  /**
   * @param id undefined
   */
  ApiUserRestoreByIdGet(id: string): __Observable<null> {
    return this.ApiUserRestoreByIdGetResponse(id).pipe(
      __map(_r => _r.body as null)
    );
  }
}

module UserService {
}

export { UserService }

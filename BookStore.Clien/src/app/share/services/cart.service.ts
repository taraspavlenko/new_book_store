import { Injectable } from '@angular/core';
import { GetBookView } from '../models/get-book-view';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  bookArray: GetBookView[] = [];
  constructor() { }

  setToStorage(book: GetBookView) {
    const booksList = localStorage.getItem('books');
    if (booksList != null) {
      this.bookArray = JSON.parse(booksList);
    }
    this.bookArray.push(book);

    localStorage.removeItem('books');
    localStorage.setItem('books', JSON.stringify(this.bookArray));
  }
  getFromStorage(): GetBookView[] {
    return JSON.parse(localStorage.getItem('books'));
  }
}

/* tslint:disable */
import { BookGetBooksViewItem } from './book-get-books-view-item';
export interface GetBooksView {
  books?: Array<BookGetBooksViewItem>;
}

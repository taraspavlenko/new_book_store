/* tslint:disable */
export interface UpdateUserView {
  id?: string;
  userName?: string;
  email?: string;
  phoneNumber?: string;
  password?: string;
}

/* tslint:disable */
export interface GenreGetBookViewItem {
  id?: string;
  title?: string;
}

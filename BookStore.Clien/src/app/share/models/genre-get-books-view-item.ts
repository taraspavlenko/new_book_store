/* tslint:disable */
export interface GenreGetBooksViewItem {
  id?: string;
  title?: string;
}

/* tslint:disable */
export interface CreateBookView {
  title?: string;
  price?: number;
  url?: string;
  description?: string;
  genresId?: Array<string>;
  authorsId?: Array<string>;
  publishersId?: Array<string>;
}

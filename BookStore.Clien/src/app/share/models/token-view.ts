/* tslint:disable */
export interface TokenView {
  accessToken?: string;
  refreshToken?: string;
}

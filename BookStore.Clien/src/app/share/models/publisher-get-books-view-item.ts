/* tslint:disable */
export interface PublisherGetBooksViewItem {
  id?: string;
  title?: string;
}

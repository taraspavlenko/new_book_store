/* tslint:disable */
import { AuthorGetBookViewItem } from './author-get-book-view-item';
import { GenreGetBookViewItem } from './genre-get-book-view-item';
import { PublisherGetBookViewItem } from './publisher-get-book-view-item';
export interface GetBookView {
  id?: string;
  creationDate?: string;
  modificationDate?: string;
  title?: string;
  price?: number;
  url?: string;
  description?: string;
  authors?: Array<AuthorGetBookViewItem>;
  genres?: Array<GenreGetBookViewItem>;
  publishers?: Array<PublisherGetBookViewItem>;
}

/* tslint:disable */
export interface UpdateAuthorView {
  id?: string;
  firstName?: string;
  lastName?: string;
}

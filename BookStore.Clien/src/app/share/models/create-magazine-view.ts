/* tslint:disable */
export interface CreateMagazineView {
  title?: string;
  price?: number;
  url?: string;
  description?: string;
  booksId?: Array<string>;
}

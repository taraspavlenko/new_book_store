/* tslint:disable */
export interface UpdateMagazineView {
  id?: string;
  title?: string;
  price?: number;
  url?: string;
  description?: string;
  booksId?: Array<string>;
}

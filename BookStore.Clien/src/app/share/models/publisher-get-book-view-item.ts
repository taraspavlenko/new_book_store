/* tslint:disable */
export interface PublisherGetBookViewItem {
  id?: string;
  title?: string;
}

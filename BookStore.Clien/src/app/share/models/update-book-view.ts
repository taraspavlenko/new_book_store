/* tslint:disable */
export interface UpdateBookView {
  id?: string;
  title?: string;
  price?: number;
  url?: string;
  description?: string;
  genresId?: Array<string>;
  authorsId?: Array<string>;
  publishersId?: Array<string>;
}

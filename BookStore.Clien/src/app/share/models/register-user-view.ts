/* tslint:disable */
export interface RegisterUserView {
  userName?: string;
  email?: string;
  phoneNumber?: string;
  password?: string;
  content?: string;
}

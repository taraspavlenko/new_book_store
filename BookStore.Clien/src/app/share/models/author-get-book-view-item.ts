/* tslint:disable */
export interface AuthorGetBookViewItem {
  id?: string;
  firstName?: string;
  lastName?: string;
}

/* tslint:disable */
import { AuthorGetBooksViewItem } from './author-get-books-view-item';
import { GenreGetBooksViewItem } from './genre-get-books-view-item';
import { PublisherGetBooksViewItem } from './publisher-get-books-view-item';
export interface BookGetBooksViewItem {
  id?: string;
  creationDate?: string;
  modificationDate?: string;
  title?: string;
  price?: number;
  url?: string;
  description?: string;
  authors?: Array<AuthorGetBooksViewItem>;
  genres?: Array<GenreGetBooksViewItem>;
  publishers?: Array<PublisherGetBooksViewItem>;
}

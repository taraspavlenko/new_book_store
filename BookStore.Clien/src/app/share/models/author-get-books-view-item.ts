/* tslint:disable */
export interface AuthorGetBooksViewItem {
  id?: string;
  firstName?: string;
  lastName?: string;
}

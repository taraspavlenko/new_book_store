/* tslint:disable */
import { NgModule, ModuleWithProviders } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ApiConfiguration, ApiConfigurationInterface } from './api-configuration';

import { AccountService } from './services/account.service';
import { AuthorService } from './services/author.service';
import { BookService } from './services/book.service';
import { GenreService } from './services/genre.service';
import { MagazineService } from './services/magazine.service';
import { PublisherService } from './services/publisher.service';
import { RoleService } from './services/role.service';
import { UserService } from './services/user.service';

/**
 * Provider for all Api services, plus ApiConfiguration
 */
@NgModule({
  imports: [
    HttpClientModule
  ],
  exports: [
    HttpClientModule
  ],
  declarations: [],
  providers: [
    ApiConfiguration,
    AccountService,
    AuthorService,
    BookService,
    GenreService,
    MagazineService,
    PublisherService,
    RoleService,
    UserService
  ],
})
export class ApiModule {
  static forRoot(customParams: ApiConfigurationInterface): ModuleWithProviders {
    return {
      ngModule: ApiModule,
      providers: [
        {
          provide: ApiConfiguration,
          useValue: {rootUrl: customParams.rootUrl}
        }
      ]
    }
  }
}

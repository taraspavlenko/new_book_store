import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AlertifyService } from '../share/services/alertify.service';
import { AccountService } from '../share/services';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  isLogin = false;

  constructor(private accountService: AccountService, private alertify: AlertifyService, private router: Router) { }

  ngOnInit() {
    this.createForm();
  }

  private createForm() {
    this.loginForm = new FormGroup({
      userName: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
    });
  }
  public login() {
    this.accountService.ApiAccountLoginPost(this.loginForm.value).subscribe(next => {
      this.alertify.success('Authenticated');
      this.isLogin = true;
      this.router.navigate(['/home']);
    }, error => {
      this.alertify.error(error);
    });
  }
  public logout() {
    localStorage.removeItem('accessToken');
    localStorage.removeItem('refreshToken');
    this.router.navigate(['/home']);
    this.isLogin = false;
    this.alertify.success('Logged out successfully');

  }

}
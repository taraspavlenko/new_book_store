import { Component, OnInit } from '@angular/core';
import { AlertifyService } from '../share/services/alertify.service';
import { GetBooksView, GetBookView, BookGetBooksViewItem } from '../share/models';
import { BookService } from '../share/services';
import { CartService } from '../share/services/cart.service';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {

  bookList: GetBookView[];
  bookItem: GetBookView;
  bookId = '52d2188e-b935-4b90-8830-822db4dbea2f';
  constructor(private productsService: BookService, private alertifyService: AlertifyService, private cartService: CartService) { }
  ngOnInit() {
    this.getBooks();
  }

  getBooks() {
    this.productsService.ApiBookGetGet().subscribe((books: GetBooksView) => {
      this.bookList = books.books;
      console.log(books);
    });
  }
  getBook() {
    this.productsService.ApiBookGetByIdGetResponse(this.bookId).subscribe((book: GetBookView) => {
      this.bookItem = book;
      console.log(this.bookItem);
    });
  }
  addToCart(book: GetBookView) {
    this.alertifyService.success('Added to cart');
    this.cartService.setToStorage(book);
  }
}

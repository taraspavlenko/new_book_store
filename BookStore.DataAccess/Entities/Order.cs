﻿using BookStore.DataAccessLayer.Entities.Base;
using Dapper.Contrib.Extensions;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace BookStore.DataAccessLayer.Entities
{
    public class Order : BaseEntity
    {
        public bool Confirmed { get; set; }
        public bool Payed { get; set; }

        public double TotalPrice { get; set; }

       
        public string UserId { get; set; }
        public User User{ get; set; }

        [NotMapped]
        [Computed]
        public virtual ICollection<OrderBook> OrderBooks { get; set; }
        [NotMapped]
        [Computed]
        public virtual ICollection<OrderMagazine> OrderMagazines { get; set; }
    }
}

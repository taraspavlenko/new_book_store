﻿using BookStore.DataAccessLayer.Entities.Base;
using Dapper.Contrib.Extensions;

namespace BookStore.DataAccessLayer.Entities
{
    public class BookGenre : BaseEntity
    {
        public string BookId { get; set; }
        public string GenreId { get; set; }

        [Write(false)]
        public Book Book { get; set; }
        [Write(false)]
        public Genre Genre { get; set; }
    }
}

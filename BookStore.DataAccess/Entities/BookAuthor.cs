﻿using BookStore.DataAccessLayer.Entities.Base;
using Dapper.Contrib.Extensions;

namespace BookStore.DataAccessLayer.Entities
{
    public class BookAuthor : BaseEntity
    {
        public string BookId { get; set; }
        public string AuthorId { get; set; }

        [Write(false)]
        public virtual Book Book { get; set; }
        [Write(false)]
        public virtual Author Author { get; set; }
    }
}

﻿using BookStore.DataAccessLayer.Entities.Base;

namespace BookStore.DataAccessLayer.Entities
{
    public class Publisher : BaseEntity
    {
        public string Title { get; set; }
    }
}

﻿using BookStore.DataAccessLayer.Entities.Base;
using Dapper.Contrib.Extensions;

namespace BookStore.DataAccessLayer.Entities
{
    public class MagazineBook : BaseEntity
    {
        public string MagazineId { get; set; }
        public string BookId { get; set; }

        [Write(false)]
        public Magazine Magazine { get; set; }
        [Write(false)]
        public Book Book { get; set; }
    }
}

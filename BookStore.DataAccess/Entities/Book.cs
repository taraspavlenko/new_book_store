﻿using BookStore.DataAccessLayer.Entities.Base;
using Dapper.Contrib.Extensions;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace BookStore.DataAccessLayer.Entities
{
    public class Book : BaseEntity
    {
        public string Title { get; set; }
        public double Price { get; set; }
        public string Url { get; set; }
        public string Description { get; set; }

        [NotMapped]
        [Computed]
        public virtual ICollection<BookAuthor> BookAuthors { get; set; }
        [NotMapped]
        [Computed]
        public virtual ICollection<BookGenre> BookGenres { get; set; }
        [NotMapped]
        [Computed]
        public virtual ICollection<BookPublisher> BookPublishers { get; set; }
        [NotMapped]
        [Computed]
        public virtual ICollection<MagazineBook> MagazineBooks { get; set; }
        
    }
}

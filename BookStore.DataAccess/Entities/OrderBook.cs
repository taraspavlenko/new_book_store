﻿using BookStore.DataAccessLayer.Entities.Base;
using Dapper.Contrib.Extensions;

namespace BookStore.DataAccessLayer.Entities
{
    public class OrderBook : BaseEntity
    {
        public string OrderId { get; set; }
        public string BookId { get; set; }

        [Write(false)]
        public Order Order { get; set; }
        [Write(false)]
        public Book Book { get; set; }
    }
}

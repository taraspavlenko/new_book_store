﻿using BookStore.DataAccessLayer.Entities.Base;
using Dapper.Contrib.Extensions;

namespace BookStore.DataAccessLayer.Entities
{
    public class OrderMagazine : BaseEntity
    {
        public string OrderId { get; set; }
        public string MagazineId { get; set; }

        [Write(false)]
        public Order Order { get; set; }
        [Write(false)]
        public Magazine Magazine { get; set; }
    }
}

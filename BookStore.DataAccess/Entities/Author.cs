﻿using BookStore.DataAccessLayer.Entities.Base;

namespace BookStore.DataAccessLayer.Entities
{
    public class Author : BaseEntity 
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}

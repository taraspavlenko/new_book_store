﻿using Dapper.Contrib.Extensions;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace BookStore.DataAccessLayer.Entities
{
    public class User : IdentityUser
    {
        public bool Deleted { get; set; }
        [NotMapped]
        [Computed]
        public virtual ICollection<Order> Orders { get; set; }
        [NotMapped]
        [Computed]
        public virtual ICollection<string> Roles { get; set; }
    }
}
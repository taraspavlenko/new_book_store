﻿using Dapper.Contrib.Extensions;
using System;

namespace BookStore.DataAccessLayer.Entities.Base
{
    public class BaseEntity
    {
        public BaseEntity()
        {
            Id = Guid.NewGuid().ToString();
            CreationDate = DateTime.UtcNow;
        }
        [ExplicitKey]
        public string Id { get; private set; }
        public DateTime CreationDate { get; private set; }
        public DateTime? ModificationDate { get; set; }
        public bool Deleted { get; set; }
    }

}

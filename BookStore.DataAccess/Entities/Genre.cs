﻿using BookStore.DataAccessLayer.Entities.Base;

namespace BookStore.DataAccessLayer.Entities
{
    public class Genre : BaseEntity
    {
        public string Title { get; set; }
    }
}

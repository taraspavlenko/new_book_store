﻿using BookStore.DataAccessLayer.Entities.Base;
using Dapper.Contrib.Extensions;

namespace BookStore.DataAccessLayer.Entities
{
    public class BookPublisher : BaseEntity
    {
        public string BookId { get; set; }
        public string PublisherId { get; set; }

        [Write(false)]
        public Book Book { get; set; }
        [Write(false)]
        public Publisher Publisher { get; set; }
    }
}

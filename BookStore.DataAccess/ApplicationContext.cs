﻿using BookStore.DataAccessLayer.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace BookStore.DataAccess
{
    public class ApplicationContext : IdentityDbContext<User>
    {
        public ApplicationContext(DbContextOptions<ApplicationContext> options) : base(options) { }

        public DbSet<Author> Authors { get; set; }
        public DbSet<Book> Books { get; set; }
        public DbSet<Genre> Genres { get; set; }
        public DbSet<Magazine> Magazines { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Publisher> Publishers { get; set; }

        public DbSet<BookAuthor> BookAuthors { get; set; }
        public DbSet<BookGenre> BookGenres { get; set; }
        public DbSet<BookPublisher> BookPublishers { get; set; }
        public DbSet<MagazineBook> MagazineBooks { get; set; }
        public DbSet<OrderBook> OrderBooks { get; set; }
        public DbSet<OrderMagazine> OrderMagazines { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<BookGenre>()
             .HasOne(b => b.Book)
             .WithMany(b => b.BookGenres)
             .HasForeignKey(b => b.BookId)
             .IsRequired()
             .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<BookAuthor>()
             .HasOne(b => b.Book)
             .WithMany(b => b.BookAuthors)
             .HasForeignKey(b => b.BookId)
             .IsRequired()
             .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<BookPublisher>()
             .HasOne(b => b.Book)
             .WithMany(b => b.BookPublishers)
             .HasForeignKey(b => b.BookId)
             .IsRequired()
             .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<MagazineBook>()
            .HasOne(b => b.Magazine)
            .WithMany(b => b.MagazineBooks)
            .HasForeignKey(b => b.MagazineId)
            .IsRequired()
            .OnDelete(DeleteBehavior.Cascade);
        }

    }
}

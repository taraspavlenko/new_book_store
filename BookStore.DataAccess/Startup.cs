﻿using BookStore.DataAccess.Repositories.EFRepositories;
using BookStore.DataAccess.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace BookStore.DataAccess
{
    public static class Startup
    {
        private static string _connectionString;
        public static void ConfigureServices(IServiceCollection services, string connectionString)
        {
            _connectionString = connectionString;
            services.AddDbContext<ApplicationContext>(op => op.UseSqlServer(connectionString));

            services.AddScoped<IBookRepository, EFBookRepository>();
            services.AddScoped<IAuthorRepository, EFAuthorRepository>();
            services.AddScoped<IGenreRepository, EFGenreRepository>();
            services.AddScoped<IPublisherRepository, EFPublisherRepository>();
            services.AddScoped<IMagazineRepository, EFMagazineRepository>();
            services.AddScoped<IOrderRepository, EFOrderRepository>();

            services.AddScoped<IBookAuthorRepository, EFBookAuthorRepository>();
            services.AddScoped<IBookPublisherRepository, EFBookPublisherRepository>();
            services.AddScoped<IBookGenreRepository, EFBookGenreRepository>();
            services.AddScoped<IMagazineBookRepository, EFMagazineBookRepository>();
        }
        public static string GetConnectionString()
        {
            return _connectionString;
        }
    }
}

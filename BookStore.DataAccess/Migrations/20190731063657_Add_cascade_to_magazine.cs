﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BookStore.DataAccess.Migrations
{
    public partial class Add_cascade_to_magazine : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MagazineBooks_Magazines_MagazineId",
                table: "MagazineBooks");

            migrationBuilder.AlterColumn<string>(
                name: "MagazineId",
                table: "MagazineBooks",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_MagazineBooks_Magazines_MagazineId",
                table: "MagazineBooks",
                column: "MagazineId",
                principalTable: "Magazines",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MagazineBooks_Magazines_MagazineId",
                table: "MagazineBooks");

            migrationBuilder.AlterColumn<string>(
                name: "MagazineId",
                table: "MagazineBooks",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AddForeignKey(
                name: "FK_MagazineBooks_Magazines_MagazineId",
                table: "MagazineBooks",
                column: "MagazineId",
                principalTable: "Magazines",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

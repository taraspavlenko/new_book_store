﻿using BookStore.DataAccess.Repositories.Interfaces;
using Dapper;
using Dapper.Contrib.Extensions;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore.DataAccess.Repositories.DapperRepositories.Base
{
    public class DapperGenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class
    {
        private readonly string _connectionString;

        public DapperGenericRepository()
        {
            _connectionString = Startup.GetConnectionString();
        }
        public virtual async Task InsertAsync(TEntity item)
        {
            using (IDbConnection dbConnection = new SqlConnection(_connectionString))
            {
                await dbConnection.InsertAsync(item);
            }
        }
        public virtual async Task<List<TEntity>> GetAllAsync()
        {
            using (IDbConnection dbConnection = new SqlConnection(_connectionString))
            {
                var result = await dbConnection.GetAllAsync<TEntity>();
                return result.ToList();
            }
        }
        public virtual async Task<TEntity> GetByIdAsync(string id)
        {
            using (IDbConnection dbConnection = new SqlConnection(_connectionString))
            {
                return await dbConnection.GetAsync<TEntity>(id);
            }
        }
        public virtual async Task UpdateAsync(TEntity item)
        {
            using (IDbConnection dbConnection = new SqlConnection(_connectionString))
            {
                await dbConnection.UpdateAsync(item);
            }
        }
    }
}

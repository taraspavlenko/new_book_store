﻿using BookStore.DataAccess.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BookStore.DataAccess.Repositories.EFRepositories.Base
{
    public class EFGeneticRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class
    {
        private readonly ApplicationContext _context;
        private DbSet<TEntity> _tableEntity;
        public EFGeneticRepository(ApplicationContext context)
        {
            _context = context;
            _tableEntity = _context.Set<TEntity>();
        }
        public virtual async Task InsertAsync(TEntity item)
        {
            await _tableEntity.AddAsync(item);
            await _context.SaveChangesAsync();
        }

        public virtual async Task Delete(string id)
        {
            var item = await GetByIdAsync(id);
            _tableEntity.Remove(item);
            await _context.SaveChangesAsync();
        }

        public virtual async Task<List<TEntity>> GetAllAsync()
        {
            return await _tableEntity.ToListAsync();
        }

        public virtual async Task<TEntity> GetByIdAsync(string id)
        {
            return await _tableEntity.FindAsync(id);
        }
        public virtual async Task UpdateAsync(TEntity item)
        {
            _tableEntity.Update(item);
            await _context.SaveChangesAsync();
        }
    }
}

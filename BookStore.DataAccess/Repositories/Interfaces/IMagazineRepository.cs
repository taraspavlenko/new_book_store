﻿using BookStore.DataAccessLayer.Entities;

namespace BookStore.DataAccess.Repositories.Interfaces
{
    public interface IMagazineRepository : IGenericRepository<Magazine>
    {
    }
}

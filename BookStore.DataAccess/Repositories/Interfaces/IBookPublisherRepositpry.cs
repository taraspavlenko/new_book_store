﻿using BookStore.DataAccessLayer.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BookStore.DataAccess.Repositories.Interfaces
{
    public interface IBookPublisherRepository : IGenericRepository<BookPublisher>
    {
        Task InsertRangeAsync(ICollection<BookPublisher> bookPublishersList);
        void DeleteRange(ICollection<BookPublisher> bookPublishersList);
        Task<ICollection<BookPublisher>> GetByBookIdAsync(string id);
    }
}

﻿using BookStore.DataAccessLayer.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BookStore.DataAccess.Repositories.Interfaces
{
    public interface IBookAuthorRepository : IGenericRepository<BookAuthor>
    {
        Task InsertRangeAsync(ICollection<BookAuthor> bookAuthorsList);
        void DeleteRange(ICollection<BookAuthor> bookAuthorsList);
        Task<ICollection<BookAuthor>> GetByBookIdAsync(string bookId);
    }
}
 

﻿using BookStore.DataAccessLayer.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BookStore.DataAccess.Repositories.Interfaces
{
    public interface IMagazineBookRepository : IGenericRepository<MagazineBook>
    {
        Task InsertRangeAsync(ICollection<MagazineBook> magazineBookList);
        void DeleteRange(ICollection<MagazineBook> magazineBookList);
        Task<ICollection<MagazineBook>> GetByMagazineIdAsync(string id);
    }
}

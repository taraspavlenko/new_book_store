﻿using BookStore.DataAccessLayer.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BookStore.DataAccess.Repositories.Interfaces
{
    public interface IBookGenreRepository : IGenericRepository<BookGenre>
    {
        Task InsertRangeAsync(ICollection<BookGenre> bookGenresList);
        void DeleteRange(ICollection<BookGenre> bookGenresList);
        Task<ICollection<BookGenre>> GetByBookIdAsync(string id);
    }
}

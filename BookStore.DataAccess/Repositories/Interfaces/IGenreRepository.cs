﻿using BookStore.DataAccessLayer.Entities;

namespace BookStore.DataAccess.Repositories.Interfaces
{
    public interface IGenreRepository : IGenericRepository<Genre>
    {
    }
}

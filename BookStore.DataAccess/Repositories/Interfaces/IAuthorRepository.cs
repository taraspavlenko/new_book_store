﻿using BookStore.DataAccessLayer.Entities;

namespace BookStore.DataAccess.Repositories.Interfaces
{
    public interface IAuthorRepository : IGenericRepository<Author>
    {
    }
}

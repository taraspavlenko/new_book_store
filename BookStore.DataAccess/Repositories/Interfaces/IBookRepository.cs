﻿using BookStore.DataAccessLayer.Entities;

namespace BookStore.DataAccess.Repositories.Interfaces
{
    public interface IBookRepository : IGenericRepository<Book>
    {
    }
}

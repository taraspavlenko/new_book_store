﻿using BookStore.DataAccess.Repositories.EFRepositories.Base;
using BookStore.DataAccess.Repositories.Interfaces;
using BookStore.DataAccessLayer.Entities;

namespace BookStore.DataAccess.Repositories.EFRepositories
{
    public class EFAuthorRepository : EFGeneticRepository<Author>, IAuthorRepository
    {
        public EFAuthorRepository(ApplicationContext context) : base(context){}
    }
}

﻿using BookStore.DataAccess.Repositories.EFRepositories.Base;
using BookStore.DataAccess.Repositories.Interfaces;
using BookStore.DataAccessLayer.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore.DataAccess.Repositories.EFRepositories
{
    public class EFBookAuthorRepository : EFGeneticRepository<BookAuthor>, IBookAuthorRepository
    {
        private readonly ApplicationContext _context;
        public EFBookAuthorRepository(ApplicationContext context) : base(context)
        {
            _context = context;
        }
        public override async Task<List<BookAuthor>> GetAllAsync()
        {
            return await _context.BookAuthors.Include(a => a.Author).ToListAsync();
        }
        public async Task InsertRangeAsync(ICollection<BookAuthor> bookAuthorsList)
        {
            await _context.BookAuthors.AddRangeAsync(bookAuthorsList);
            await _context.SaveChangesAsync();
        }
        public void DeleteRange(ICollection<BookAuthor> bookAuthorsList)
        {
            _context.BookAuthors.RemoveRange(bookAuthorsList);
            _context.SaveChangesAsync();
        }
        public async Task<ICollection<BookAuthor>> GetByBookIdAsync(string bookId)
        {
            return await _context.BookAuthors.Where(i => i.BookId == bookId).Include(a => a.Author).ToListAsync();
        }
    }
}

﻿using BookStore.DataAccess.Repositories.EFRepositories.Base;
using BookStore.DataAccess.Repositories.Interfaces;
using BookStore.DataAccessLayer.Entities;

namespace BookStore.DataAccess.Repositories.EFRepositories
{
    public class EFOrderRepository : EFGeneticRepository<Order>, IOrderRepository
    {
        public EFOrderRepository(ApplicationContext context) : base(context)
        {
        }
    }
}

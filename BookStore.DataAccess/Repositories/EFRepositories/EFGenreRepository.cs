﻿using BookStore.DataAccess.Repositories.EFRepositories.Base;
using BookStore.DataAccess.Repositories.Interfaces;
using BookStore.DataAccessLayer.Entities;

namespace BookStore.DataAccess.Repositories.EFRepositories
{
    public class EFGenreRepository : EFGeneticRepository<Genre>, IGenreRepository
    {
        public EFGenreRepository(ApplicationContext context) : base(context){}
    }
}

﻿using BookStore.DataAccess.Repositories.EFRepositories.Base;
using BookStore.DataAccess.Repositories.Interfaces;
using BookStore.DataAccessLayer.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore.DataAccess.Repositories.EFRepositories
{
    public class EFMagazineBookRepository : EFGeneticRepository<MagazineBook>, IMagazineBookRepository
    {
        private readonly ApplicationContext _context;

        public EFMagazineBookRepository(ApplicationContext context) : base(context)
        {
            _context = context;
        }
        public override async Task<List<MagazineBook>> GetAllAsync()
        {
            return await _context.MagazineBooks.Include(x => x.Book).ToListAsync();
        }
        public void DeleteRange(ICollection<MagazineBook> magazineBookList)
        {
            _context.MagazineBooks.RemoveRange(magazineBookList);
            _context.SaveChangesAsync();
        }

        public async Task InsertRangeAsync(ICollection<MagazineBook> magazineBookList)
        {
            await _context.MagazineBooks.AddRangeAsync(magazineBookList);
            await _context.SaveChangesAsync();
        }

        public async Task<ICollection<MagazineBook>> GetByMagazineIdAsync(string id)
        {
            return await _context.MagazineBooks.Where(i => i.MagazineId == id).Include(x => x.Book).ToListAsync();
        }
    }
}

﻿using BookStore.DataAccess.Repositories.EFRepositories.Base;
using BookStore.DataAccess.Repositories.Interfaces;
using BookStore.DataAccessLayer.Entities;

namespace BookStore.DataAccess.Repositories.EFRepositories
{
    public class EFPublisherRepository : EFGeneticRepository<Publisher>, IPublisherRepository
    {
        public EFPublisherRepository(ApplicationContext context) : base(context){}
    }
}

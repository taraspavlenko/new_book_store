﻿using BookStore.DataAccess.Repositories.EFRepositories.Base;
using BookStore.DataAccess.Repositories.Interfaces;
using BookStore.DataAccessLayer.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore.DataAccess.Repositories.EFRepositories
{
    public class EFBookGenreRepository : EFGeneticRepository<BookGenre>, IBookGenreRepository
    {
        private readonly ApplicationContext _context;

        public EFBookGenreRepository(ApplicationContext context) : base(context)
        {
            _context = context;
        }

        public async Task InsertRangeAsync(ICollection<BookGenre> listBookGenre)
        {
            await _context.BookGenres.AddRangeAsync(listBookGenre);
            await _context.SaveChangesAsync();
        }
        public override async Task<List<BookGenre>> GetAllAsync()
        {
            return await _context.BookGenres.Include(g => g.Genre).ToListAsync();
        }

        public void DeleteRange(ICollection<BookGenre> bookGenresList)
        {
            _context.BookGenres.RemoveRange(bookGenresList);
            _context.SaveChangesAsync();
        }

        public async Task<ICollection<BookGenre>> GetByBookIdAsync(string bookId)
        {
            return await _context.BookGenres.Where(i => i.BookId == bookId).Include(g => g.Genre).ToListAsync();
        }
    }
}

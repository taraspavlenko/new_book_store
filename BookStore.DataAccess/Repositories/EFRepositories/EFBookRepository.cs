﻿using BookStore.DataAccess.Repositories.EFRepositories.Base;
using BookStore.DataAccess.Repositories.Interfaces;
using BookStore.DataAccessLayer.Entities;

namespace BookStore.DataAccess.Repositories.EFRepositories
{
    public class EFBookRepository : EFGeneticRepository<Book>, IBookRepository
    {
        public EFBookRepository(ApplicationContext context) : base(context)
        {
        }
    }
}

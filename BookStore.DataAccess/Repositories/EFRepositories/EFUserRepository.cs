﻿using BookStore.DataAccess.Repositories.EFRepositories.Base;
using BookStore.DataAccess.Repositories.Interfaces;
using BookStore.DataAccessLayer.Entities;

namespace BookStore.DataAccess.Repositories.EFRepositories
{
    public class EFUserRepository : EFGeneticRepository<User>, IUserRepository
    {
        public EFUserRepository(ApplicationContext context) : base(context)
        {
        }

    }
}

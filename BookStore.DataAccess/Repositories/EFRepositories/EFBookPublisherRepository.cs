﻿using BookStore.DataAccess.Repositories.EFRepositories.Base;
using BookStore.DataAccess.Repositories.Interfaces;
using BookStore.DataAccessLayer.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore.DataAccess.Repositories.EFRepositories
{
    public class EFBookPublisherRepository : EFGeneticRepository<BookPublisher>, IBookPublisherRepository
    {
        private readonly ApplicationContext _context;

        public EFBookPublisherRepository(ApplicationContext context) : base(context)
        {
            _context = context;
        }
        public async Task InsertRangeAsync(ICollection<BookPublisher> listBookPublisher)
        {
            await _context.BookPublishers.AddRangeAsync(listBookPublisher);
            await _context.SaveChangesAsync();
        }
        public override async Task<List<BookPublisher>> GetAllAsync()
        {
            return await _context.BookPublishers.Include(a => a.Publisher).ToListAsync();
        }

        public void DeleteRange(ICollection<BookPublisher> bookPublishersList)
        {
            _context.BookPublishers.RemoveRange(bookPublishersList);
            _context.SaveChangesAsync();
        }

        public async Task<ICollection<BookPublisher>> GetByBookIdAsync(string bookId)
        {
            return await _context.BookPublishers.Where(i => i.BookId == bookId).Include(a => a.Publisher).ToListAsync();
        }
    }
}

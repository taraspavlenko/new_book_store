﻿using BookStore.DataAccess.Repositories.EFRepositories.Base;
using BookStore.DataAccess.Repositories.Interfaces;
using BookStore.DataAccessLayer.Entities;

namespace BookStore.DataAccess.Repositories.EFRepositories
{
    public class EFMagazineRepository : EFGeneticRepository<Magazine>, IMagazineRepository
    {
        public EFMagazineRepository(ApplicationContext context) : base(context)
        {
        }
    }
}

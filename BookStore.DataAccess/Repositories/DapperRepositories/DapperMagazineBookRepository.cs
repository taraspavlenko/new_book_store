﻿using BookStore.DataAccess.Repositories.DapperRepositories.Base;
using BookStore.DataAccess.Repositories.Interfaces;
using BookStore.DataAccessLayer.Entities;
using Dapper;
using Dapper.Contrib.Extensions;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore.DataAccess.Repositories.DapperRepositories
{
    public class DapperMagazineBookRepository : DapperGenericRepository<MagazineBook>, IMagazineBookRepository
    {
        private readonly string _connectionString;
        public DapperMagazineBookRepository()
        {
            _connectionString = Startup.GetConnectionString();
        }

        public override async Task<List<MagazineBook>> GetAllAsync()
        {
            string sql = @" SELECT  * 
                            FROM 
                                    MagazineBooks magazineBook 
                            INNER JOIN 
                                    Books book 
                            ON magazineBook.BookId = book.Id";

            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                var magazineBooks = await connection.QueryAsync<MagazineBook, Book, MagazineBook>(sql, (magazineBook, book) =>
                {
                    magazineBook.Book = book;
                    return magazineBook;
                },
                splitOn: "Id"
                );
                return magazineBooks.ToList();
            }
        }
        public void DeleteRange(ICollection<MagazineBook> magazineBookList)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                connection.Delete(magazineBookList);
            }
        }

        public async Task InsertRangeAsync(ICollection<MagazineBook> magazineBookList)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                await connection.InsertAsync(magazineBookList);
            }
        }
        public async Task<ICollection<MagazineBook>> GetByMagazineIdAsync(string id)
        {
            string sql = @" SELECT  * 
                            FROM 
                                    MagazineBooks magazineBook 
                            INNER JOIN 
                                    Books book 
                            ON 
                                    magazineBook.BookId = book.Id
                            WHERE
                                    magazineBook.MagazineId = @magazineId";

            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                var magazineBooks = await connection.QueryAsync<MagazineBook, Book, MagazineBook>(sql, (magazineBook, book) =>
                {
                    magazineBook.Book = book;
                    return magazineBook;
                }, new { magazineId = id },
                splitOn: "Id"
                );
                return magazineBooks.ToList();
            }
        }
    }
}

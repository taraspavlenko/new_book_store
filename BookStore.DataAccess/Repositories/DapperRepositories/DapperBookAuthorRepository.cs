﻿using BookStore.DataAccess.Repositories.DapperRepositories.Base;
using BookStore.DataAccess.Repositories.Interfaces;
using BookStore.DataAccessLayer.Entities;
using Dapper;
using Dapper.Contrib.Extensions;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore.DataAccess.Repositories.DapperRepositories
{
    public class DapperBookAuthorRepository : DapperGenericRepository<BookAuthor>, IBookAuthorRepository
    {
        private readonly string _connectionString;
        public DapperBookAuthorRepository()
        {
            _connectionString = Startup.GetConnectionString();
        }

        public override async Task<List<BookAuthor>> GetAllAsync()
        {
            string sql = @" SELECT  * 
                            FROM 
                                    BookAuthors bookAuthor 
                            INNER JOIN 
                                    Authors author 
                            ON bookAuthor.AuthorId = author.Id";

            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                var bookAuthors = await connection.QueryAsync<BookAuthor, Author, BookAuthor>(sql, (bookAuthor, author) =>
                {
                    bookAuthor.Author = author;
                    return bookAuthor;
                },
                splitOn: "Id"
                );
                return bookAuthors.ToList();
            }
        }
        public async Task<ICollection<BookAuthor>> GetByBookIdAsync(string id)
        {
            string sql = @" SELECT  * 
                            FROM 
                                    BookAuthors bookAuthor 
                            INNER JOIN 
                                    Authors author 
                            ON 
                                    bookAuthor.AuthorId = author.Id
                            WHERE
                                    bookAuthor.BookId = @bookId";
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                var bookAuthors = await connection.QueryAsync<BookAuthor, Author, BookAuthor>(sql, (bookAuthor, author) =>
                {
                    bookAuthor.Author = author;
                    return bookAuthor;
                }, new { bookId = id},
                splitOn: "Id"
                );
                return bookAuthors.ToList();
            }
        }
        public async Task InsertRangeAsync(ICollection<BookAuthor> bookAuthorsList)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                await connection.InsertAsync(bookAuthorsList);
            }
        }
        public void DeleteRange(ICollection<BookAuthor> bookAuthorsList)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                 connection.Delete(bookAuthorsList);
            }
        }
    }
}

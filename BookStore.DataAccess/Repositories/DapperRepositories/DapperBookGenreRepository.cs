﻿using BookStore.DataAccess.Repositories.DapperRepositories.Base;
using BookStore.DataAccess.Repositories.Interfaces;
using BookStore.DataAccessLayer.Entities;
using Dapper;
using Dapper.Contrib.Extensions;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore.DataAccess.Repositories.DapperRepositories
{
    public class DapperBookGenreRepository : DapperGenericRepository<BookGenre>, IBookGenreRepository
    {
        private readonly string _connectionString;
        public DapperBookGenreRepository()
        {
            _connectionString = Startup.GetConnectionString();
        }
        public async Task InsertRangeAsync(ICollection<BookGenre> bookGenresList)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                await connection.InsertAsync(bookGenresList);
            }
        }
        public override async Task<List<BookGenre>> GetAllAsync()
        {
            string sql = @" SELECT  * 
                            FROM 
                                    BookGenres bookGenre
                            INNER JOIN 
                                    Genres genre 
                            ON bookGenre.GenreId = genre.Id";

            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                var bookAuthors = await connection.QueryAsync<BookGenre, Genre, BookGenre>(sql, (bookGenre, genre) =>
                {
                    bookGenre.Genre = genre;
                    return bookGenre;
                },
                splitOn: "Id"
                );
                return bookAuthors.ToList();
            }
        }
        public void DeleteRange(ICollection<BookGenre> bookGenresList)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                connection.Delete(bookGenresList);
            }
        }

        public async Task<ICollection<BookGenre>> GetByBookIdAsync(string id)
        {
            string sql = @" SELECT  * 
                            FROM 
                                    BookGenres bookGenre
                            INNER JOIN 
                                    Genres genre 
                            ON 
                                    bookGenre.GenreId = genre.Id
                            WHERE
                                    bookGenre.BookId = @bookId";

            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                var bookAuthors = await connection.QueryAsync<BookGenre, Genre, BookGenre>(sql, (bookGenre, genre) =>
                {
                    bookGenre.Genre = genre;
                    return bookGenre;
                }, new { bookId = id },
                splitOn: "Id"
                );
                return bookAuthors.ToList();
            }
        }
    }
}

﻿using BookStore.DataAccess.Repositories.DapperRepositories.Base;
using BookStore.DataAccess.Repositories.Interfaces;
using BookStore.DataAccessLayer.Entities;
using Dapper;
using Dapper.Contrib.Extensions;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore.DataAccess.Repositories.DapperRepositories
{
    public class DapperBookPublisherRepository : DapperGenericRepository<BookPublisher>, IBookPublisherRepository
    {
        private readonly string _connectionString;
        public DapperBookPublisherRepository()
        {
            _connectionString = Startup.GetConnectionString();
        }
        public override async Task<List<BookPublisher>> GetAllAsync()
        {
            string sql = @" SELECT  * 
                            FROM 
                                    BookPublishers bookPublisher
                            INNER JOIN 
                                    Publishers publisher 
                            ON bookPublisher.PublisherId = publisher.Id";

            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                var bookAuthors = await connection.QueryAsync<BookPublisher, Publisher, BookPublisher>(sql, (bookPublisher, publisher) =>
                {
                    bookPublisher.Publisher = publisher;
                    return bookPublisher;
                },
                splitOn: "Id"
                );
                return bookAuthors.ToList();
            }
        }
        public async Task InsertRangeAsync(ICollection<BookPublisher> bookPublishersList)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                await connection.InsertAsync(bookPublishersList);
            }
        }
        public void DeleteRange(ICollection<BookPublisher> bookPublishersList)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                connection.Delete(bookPublishersList);
            }
        }

        public async Task<ICollection<BookPublisher>> GetByBookIdAsync(string id)
        {
            string sql = @" SELECT  * 
                            FROM 
                                    BookPublishers bookPublisher
                            INNER JOIN 
                                    Publishers publisher 
                            ON 
                                    bookPublisher.PublisherId = publisher.Id
                            WHERE
                                    bookPublisher.BookId = @bookId";
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                var bookAuthors = await connection.QueryAsync<BookPublisher, Publisher, BookPublisher>(sql, (bookPublisher, publisher) =>
                {
                    bookPublisher.Publisher = publisher;
                    return bookPublisher;
                }, new { bookId = id },
                splitOn: "Id"
                );
                return bookAuthors.ToList();
            }
        }
    }
}

﻿using BookStore.DataAccess.Repositories.DapperRepositories.Base;
using BookStore.DataAccess.Repositories.Interfaces;
using BookStore.DataAccessLayer.Entities;

namespace BookStore.DataAccess.Repositories.DapperRepositories
{
    public class DapperPublisherRepository : DapperGenericRepository<Publisher>, IPublisherRepository
    {
    }
}

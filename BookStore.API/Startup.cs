﻿using BookStore.PresentationLayer.Middlewares;
using BookStore.Services.Helpers;
using BookStore.Services.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace BookStore.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            string connectionString = Configuration.GetConnectionString("DevConnection");

            Services.Startup.ConfigureServices(services, connectionString);
            Services.Startup.ConfigureIdentity(services);
            Services.AuthOptions.SetAuthOptions(Configuration);

            services.AddScoped<IMailHelper, MailHelper>(i =>
             new MailHelper(
                Configuration["MailService:Host"],
                Configuration.GetValue<int>("MailService:Port"),
                Configuration.GetValue<bool>("MailService:EnableSSL"),
                Configuration["MailService:SenderAddress"],
                Configuration["MailService:Password"]));

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddCors();
            services.AddSwaggerGen();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseHttpStatusCodeExceptionMiddleware();
            }
            else
            {
                app.UseHttpStatusCodeExceptionMiddleware();
                app.UseHsts();
            }
            app.UseCors(x => x.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
            app.UseSwagger();
            app.UseSwaggerUi();

            app.UseAuthentication();
            app.UseMvc();
        }
    }
}

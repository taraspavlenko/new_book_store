﻿using BookStore.DataAccess.Entities;
using BookStore.Services.Interfaces;
using BookStore.Services.Views.PublisherViews;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;


namespace BookStore.PresentationLayer.Controllers
{
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = Role.Admin)]
    public class PublisherController : ControllerBase
    {
        private readonly IPublisherService _publisherService;

        public PublisherController(IPublisherService publisherService)
        {
            _publisherService = publisherService;
        }

        [HttpGet("get")]
        public async Task<IActionResult> Get()
        {
            GetPublishersView publishersView = await _publisherService.GetAllAsync();

            return Ok(publishersView);
        }

        [HttpGet("get/{id}")]
        public async Task<IActionResult> Get([FromRoute]string id)
        {
            GetPublisherView publisherView = await _publisherService.GetByIdAsync(id);
            return Ok(publisherView);
        }

        [HttpPost("create")]
        public async Task<IActionResult> Create([FromBody]CreatePublisherView publisherView)
        {
            await _publisherService.CreateAsync(publisherView);
            return Ok();
        }

        [HttpPost("update")]
        public async Task<IActionResult> Update([FromBody]UpdatePublisherView publisherView)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            await _publisherService.UpdateAsync(publisherView);
            return Ok();
        }

        [HttpGet("delete/{id}")]
        public async Task<IActionResult> Delete([FromRoute]string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new Exception("'Id' is equal null or empty");
            }
            await _publisherService.DeleteAsync(id);
            return Ok();
        }
        [HttpGet("restore/{id}")]
        public async Task<IActionResult> Restore([FromRoute]string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new Exception("'Id' is equal null or empty");
            }
            await _publisherService.RestoreAsync(id);
            return Ok();
        }
    }
}

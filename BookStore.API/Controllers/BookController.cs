﻿using BookStore.DataAccess.Entities;
using BookStore.Services.Interfaces;
using BookStore.Services.Views.BookViews;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace BookStore.PresentationLayer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = Role.Admin)]
    public class BookController : ControllerBase
    {
        private readonly IBookService _bookService;

        public BookController(IBookService bookService)
        {
            _bookService = bookService;
        }

        [HttpGet("get/{id}")]
        [AllowAnonymous]
        [ProducesResponseType(typeof(GetBookView), StatusCodes.Status200OK)]
        public async Task<IActionResult> Get([FromRoute]string id)
        {
            var bookView = await _bookService.GetByIdAsync(id);

            return Ok(bookView);
        }

        [HttpGet("get")]
        [AllowAnonymous]
        [ProducesResponseType(typeof(GetBooksView), StatusCodes.Status200OK)]
        public async Task<IActionResult> Get()
        {
            GetBooksView books = await _bookService.GetAllAsync();

            return Ok(books);
        }

        [HttpPost("create")]
        public async Task<IActionResult> Create([FromBody]CreateBookView createBookView)
        {
            await _bookService.CreateAsync(createBookView);
            return Ok();
        }

        [HttpPost("update")]
        public async Task<IActionResult> Update([FromBody]UpdateBookView updateBookView)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            await _bookService.UpdateAsync(updateBookView);
            return Ok();
        }

        [HttpGet("delete/{id}")]
        public async Task<IActionResult> Delete([FromRoute]string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return BadRequest("'id' is equal null or empty");
            }

            await _bookService.DeleteAsync(id);
            return Ok();
        }
        [HttpGet("restore/{id}")]
        public async Task<IActionResult> Restore([FromRoute]string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return BadRequest("'id' is equal null or empty");
            }

            await _bookService.RestoreAsync(id);
            return Ok();
        }
    }
}

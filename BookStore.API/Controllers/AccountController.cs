﻿using BookStore.DataAccess.Entities;
using BookStore.Services.Interfaces;
using BookStore.Services.Views;
using BookStore.Services.Views.UserViews;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace BookStore.PresentationLayer.Controllers
{
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = Role.Admin)]
    public class AccountController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IMailHelper _mailHelper;
        private readonly ITokenService _tokenService;

        public AccountController(IUserService userService,
                                 IMailHelper mailHelper,
                                 ITokenService tokenService)
        {
            _userService = userService;
            _mailHelper = mailHelper;
            _tokenService = tokenService;
        }

        [HttpPost("register")]
        [AllowAnonymous]
        public async Task<IActionResult> Register([FromBody] RegisterUserView registerUserView)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Username already exist");
            }
            var registerResult = await _userService.RegisterAsync(registerUserView);
            var emailToken = registerResult.emailToken;
            var userId = registerResult.userId;

            string callBackUrl = Url.Action(nameof(ConfirmEmail),
                                          "Account",
                                          new { UserId = userId, Token = emailToken },
                                          protocol: HttpContext.Request.Scheme);
            await _mailHelper.SendEmailAsync(registerUserView.UserName, registerUserView.Email, callBackUrl);

            return StatusCode(201);
        }
        [HttpPost("login")]
        [AllowAnonymous]
        public async Task<IActionResult> Login([FromBody] LoginView loginView)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            TokenView tokenView = await _userService.LoginAsync(loginView);

            return Ok(tokenView);
        }
        [HttpPost("refresh")]
        public async Task<IActionResult> Refresh([FromBody] TokenView tokenView)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            TokenView refreshedTokenView = await _tokenService.RefreshAsync(tokenView);

            return Ok(refreshedTokenView);
        }
        [HttpGet("confirmEmail")]
        public async Task<IActionResult> ConfirmEmail(string userId, string token)
        {
            if (userId == null || token == null)
            {
                return BadRequest("Error");
            }
            User user = await _userService.FindByIdAsync(userId);
            if (user == null)
            {
                return NotFound("Not found");
            }
            IdentityResult result = await _userService.ConfirmEmailAsyncAsync(user, token);
            return Content(result.Succeeded ? "Confirmed" : "Not confirmed");
        }
    }
}

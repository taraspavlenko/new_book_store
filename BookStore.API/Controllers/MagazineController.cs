﻿using BookStore.DataAccess.Entities;
using BookStore.Services.Interfaces;
using BookStore.Services.Views.MagazineViews;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace BookStore.PresentationLayer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = Role.Admin)]
    public class MagazineController : ControllerBase
    {
        private readonly IMagazineService _magazineService;

        public MagazineController(IMagazineService magazineService)
        {
           _magazineService = magazineService;
        }

        [HttpGet("get/{id}")]
        [AllowAnonymous]
        public async Task<IActionResult> Get([FromRoute]string id)
        {
            GetMagazineView magazineView = await _magazineService.GetByIdAsync(id);

            return Ok(magazineView);
        }

        [HttpGet("get")]
        [AllowAnonymous]
        public async Task<IActionResult> Get()
        {
            GetMagazinesView magazines = await _magazineService.GetAllAsync();

            return Ok(magazines);
        }

        [HttpPost("create")]
        public async Task<IActionResult> Create([FromBody]CreateMagazineView createMagazineView)
        {
            await _magazineService.CreateAsync(createMagazineView);
            return Ok();
        }

        [HttpGet("update")]
        public async Task<IActionResult> Update([FromBody]UpdateMagazineView updateMagazineView)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            await _magazineService.UpdateAsync(updateMagazineView);
            return Ok();
        }

        [HttpGet("delete/{id}")]
        public async Task<IActionResult> Delete([FromRoute]string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return BadRequest("'id' is equal null or empty");
            }

            await _magazineService.DeleteAsync(id);
            return Ok();
        }

        [HttpGet("restore/{id}")]
        public async Task<IActionResult> Restore([FromRoute]string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return BadRequest("'id' is equal null or empty");
            }

            await _magazineService.Restore(id);
            return Ok();
        }
    }
}

﻿using BookStore.DataAccess.Entities;
using BookStore.Services.Interfaces;
using BookStore.Services.Views.RoleViews;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace BookStore.PresentationLayer.Controllers
{
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = Role.Admin)]
    public class RoleController : ControllerBase
    {
        private readonly IRoleService _roleService;

        public RoleController(IRoleService roleService)
        {
            _roleService = roleService;
        }

        [HttpGet("get")]
        public async Task<IActionResult> Get()
        {
            GetRolesView rolesView = await _roleService.GetAllAsync();

            return Ok(rolesView);
        }

        [HttpGet("get/{id}")]
        public async Task<IActionResult> Get([FromRoute]string id)
        {
            GetRoleView roleView = await _roleService.GetByIdAsync(id);

            return Ok(roleView);
        }

        [HttpPost("create")]
        public async Task<IActionResult> Create([FromBody]CreateRoleView createRoleView)
        {
            await _roleService.CreateAsync(createRoleView);
            return Ok();
        }

        [HttpPost("update")]
        public async Task<IActionResult> Update([FromBody]UpdateRoleView updateRoleView)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            await _roleService.UpdateAsync(updateRoleView);
            return Ok();
        }
        [HttpGet("delete/{id}")]
        public async Task<IActionResult> Delete([FromRoute]string roleId)
        {
            if (string.IsNullOrEmpty(roleId))
            {
                return BadRequest("'id' is equal null or empty");
            }
            await _roleService.DeleteAsync(roleId);
            return Ok();
        }
        [HttpGet("restore/{id}")]
        public async Task<IActionResult> Restore([FromRoute]string roleId)
        {
            if (string.IsNullOrEmpty(roleId))
            {
                return BadRequest("'id' is equal null or empty");
            }
            await _roleService.RestoreAsync(roleId);
            return Ok();
        }
    }
}

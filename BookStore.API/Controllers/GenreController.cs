﻿using BookStore.DataAccess.Entities;
using BookStore.Services.Interfaces;
using BookStore.Services.Views.GenreViews;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;


namespace BookStore.PresentationLayer.Controllers
{
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = Role.Admin)]
    public class GenreController : ControllerBase
    {
        private readonly IGenreService _genreService;

        public GenreController(IGenreService genreService)
        {
            _genreService = genreService;
        }
        [HttpGet("get")]
        public async Task<IActionResult> Get()
        {
            GetGenresView genresView = await _genreService.GetAllAsync();

            return Ok(genresView);
        }

        [HttpGet("get/{id}")]
        public async Task<IActionResult> Get([FromRoute]string id)
        {
            GetGenreView genreView = await _genreService.GetByIdAsync(id);
            return Ok(genreView);
        }

        [HttpPost("create")]
        public async Task<IActionResult> Create([FromBody]CreateGenreView genreView)
        {
            await _genreService.CreateAsync(genreView);
            return Ok();
        }

        [HttpPost("update")]
        public async Task<IActionResult> Update([FromBody]UpdateGenreView genreView)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            await _genreService.UpdateAsync(genreView);
            return Ok();
        }

        [HttpGet("delete/{id}")]
        public async Task<IActionResult> Delete([FromRoute]string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new Exception("'Id' is equal null or empty");
            }
            await _genreService.DeleteAsync(id);
            return Ok();
        }
        [HttpGet("restore/{id}")]
        public async Task<IActionResult> Restore([FromRoute]string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new Exception("'Id' is equal null or empty");
            }
            await _genreService.RestoreAsync(id);
            return Ok();
        }
    }
}

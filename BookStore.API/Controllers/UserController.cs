﻿using BookStore.DataAccess.Entities;
using BookStore.Services.Interfaces;
using BookStore.Services.Views.UserViews;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace BookStore.PresentationLayer.Controllers
{
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = Role.Admin)]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }
        [HttpGet("get")]
        public async Task<IActionResult> Get()
        {
            GetUsersView usersView = await _userService.GetAllAsync();

            return Ok(usersView);
        }
        [HttpGet("get/{id}")]
        public async Task<IActionResult> Get([FromRoute]string id)
        {
            GetUserView userView = await _userService.GetByIdAsync(id);

            return Ok(userView);
        }
        [HttpPost("create")]
        public async Task<IActionResult> Create([FromBody]RegisterUserView registerUserView)
        {
            await _userService.RegisterAsync(registerUserView);
            return Ok();
        }
        [HttpPost("update")]
        public async Task<IActionResult> Update([FromBody]UpdateUserView updateUserView)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            await _userService.UpdateAsync(updateUserView);
            return Ok();
        }
        [HttpGet("delete/{id}")]
        public async Task<IActionResult> Delete([FromRoute]string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return BadRequest("'id' is equal null or empty");
            }
            await _userService.DeleteAsync(id);
            return Ok();
        }
        [HttpGet("restore/{id}")]
        public async Task<IActionResult> Restore([FromRoute]string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return BadRequest("'id' is equal null or empty");
            }
            await _userService.RestoreAsync(id);
            return Ok();
        }
    }
}

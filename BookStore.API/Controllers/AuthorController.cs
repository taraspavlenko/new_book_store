﻿using BookStore.DataAccess.Entities;
using BookStore.Services.Interfaces;
using BookStore.Services.Views.AuthorViews;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Net;
using System.Threading.Tasks;

namespace BookStore.PresentationLayer.Controllers
{
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = Role.Admin)]
    public class AuthorController : ControllerBase
    {
        private readonly IAuthorService _authorService;

        public AuthorController(IAuthorService authorService)
        {
            _authorService = authorService;
        }
        [HttpGet("get")]
        public async Task<IActionResult> Get()
        {
            GetAuthorsView authorsView = await _authorService.GetAllAsync();

            return Ok(authorsView);
        }

        [HttpGet("get/{id}")]
        public async Task<IActionResult> Get([FromRoute]string id)
        {
            GetAuthorView authorView = await _authorService.GetByIdAsync(id);

            return Ok(authorView);
        }

        [HttpPost("create")]
        public async Task<IActionResult> Create([FromBody]CreateAuthorView authorView)
        {
            await _authorService.CreateAsync(authorView);
            return Ok();
        }

        [HttpPost("update")]
        public async Task<IActionResult> Update([FromBody]UpdateAuthorView authorView)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            await _authorService.UpdateAsync(authorView);
            return Ok();
        }

        [HttpGet("delete/{id}")]
        public async Task<IActionResult> Delete([FromRoute]string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return BadRequest("'id' is equal null or empty");
            }
            await _authorService.DeleteAsync(id);
            return Ok();
        }
        [HttpGet("restore/{id}")]
        public async Task<IActionResult> Restore([FromRoute]string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return BadRequest("'id' is equal null or empty");
            }
            await _authorService.RestoreAsync(id);
            return Ok();
        }
    }
}

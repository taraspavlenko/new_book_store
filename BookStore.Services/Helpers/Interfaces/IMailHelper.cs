﻿using System.Threading.Tasks;

namespace BookStore.Services.Interfaces
{
    public interface IMailHelper
    {
        Task SendEmailAsync(string recipientName, string recipientEmail, string content);
    }
}

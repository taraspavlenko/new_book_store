﻿using System;
using System.Text;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace BookStore.Services
{
    public static class AuthOptions
    {
        public static void SetAuthOptions(IConfiguration configuration)
        {
            Key = configuration.GetSection("AuthOptions:Key").Value;
            Issuer = configuration.GetSection("AuthOptions:Issuer").Value;
            Audience = configuration.GetSection("AuthOptions:Audience").Value;
            AccessLifeTime = configuration.GetValue<double>("AuthOptions:AccessLifeTime");
            RefreshLifeTime = configuration.GetValue<double>("AuthOptions:RefreshLifeTime");
        }
        public static string Key { get; private set; }
        public static string Issuer { get; private set; }
        public static string Audience { get; private set; }
        public static double AccessLifeTime { get; private set; }
        public static double RefreshLifeTime { get; private set; }

        public static SecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Key));
        }
    }
}

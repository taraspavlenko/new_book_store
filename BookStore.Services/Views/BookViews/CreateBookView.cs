﻿using System.Collections.Generic;

namespace BookStore.Services.Views.BookViews
{
    public class CreateBookView
    {
        public string Title { get; set; }
        public double Price { get; set; }
        public string Url { get; set; }
        public string Description { get; set; }
        public List<string> GenresId { get; set; }
        public List<string> AuthorsId { get; set; }
        public List<string> PublishersId { get; set; }
    }
}

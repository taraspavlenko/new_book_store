﻿using System;
using System.Collections.Generic;

namespace BookStore.Services.Views.BookViews
{
    public class GetBooksView
    {
        public GetBooksView()
        {
            Books = new List<BookGetBooksViewItem>();
        }
        public List<BookGetBooksViewItem> Books { get; set; }
    }

    public class BookGetBooksViewItem
    {
        public string Id { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime? ModificationDate { get; set; }
        public string Title { get; set; }
        public double Price { get; set; }
        public string Url { get; set; }
        public string Description { get; set; }

        public List<AuthorGetBooksViewItem> Authors { get; set; } = new List<AuthorGetBooksViewItem>();
        public List<GenreGetBooksViewItem> Genres { get; set; } = new List<GenreGetBooksViewItem>();
        public List<PublisherGetBooksViewItem> Publishers { get; set; } = new List<PublisherGetBooksViewItem>();
    }
    public class AuthorGetBooksViewItem
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
    public class GenreGetBooksViewItem
    {
        public string Id { get; set; }
        public string Title { get; set; }
    }
    public class PublisherGetBooksViewItem
    {
        public string Id { get; set; }
        public string Title { get; set; }
    }
}

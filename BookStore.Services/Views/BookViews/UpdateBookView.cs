﻿using System.Collections.Generic;

namespace BookStore.Services.Views.BookViews
{
    public class UpdateBookView
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public double Price { get; set; }
        public string Url { get; set; }
        public string Description { get; set; }
        public List<string> GenresId { get; set; }
        public List<string> AuthorsId { get; set; }
        public List<string> PublishersId { get; set; }
    }
}

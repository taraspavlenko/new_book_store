﻿using System;
using System.Collections.Generic;

namespace BookStore.Services.Views.BookViews
{
    public class GetBookView
    {
        public string Id { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime? ModificationDate { get; set; }
        public string Title { get; set; }
        public double Price { get; set; }
        public string Url { get; set; }
        public string Description { get; set; }

        public List<AuthorGetBookViewItem> Authors { get; set; } = new List<AuthorGetBookViewItem>();
        public List<GenreGetBookViewItem> Genres { get; set; } = new List<GenreGetBookViewItem>();
        public List<PublisherGetBookViewItem> Publishers { get; set; } = new List<PublisherGetBookViewItem>();
    }

  

    public class AuthorGetBookViewItem
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
    public class GenreGetBookViewItem
    {
        public string Id { get; set; }
        public string Title { get; set; }
    }
    public class PublisherGetBookViewItem
    {
        public string Id { get; set; }
        public string Title { get; set; }
    }


}

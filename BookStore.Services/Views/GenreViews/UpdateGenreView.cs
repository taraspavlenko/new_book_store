﻿namespace BookStore.Services.Views.GenreViews
{
    public class UpdateGenreView
    {
        public string Id { get; set; }
        public string Title { get; set; }
    }
}

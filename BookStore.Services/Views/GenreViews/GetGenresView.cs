﻿using System;
using System.Collections.Generic;

namespace BookStore.Services.Views.GenreViews
{
    public class GetGenresView
    {
        public List<GenreGetGenresViewItem> Genres { get; set; }

        public GetGenresView()
        {
            Genres = new List<GenreGetGenresViewItem>();
        }
    }

    public class GenreGetGenresViewItem
    {
        public string Id { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime? ModificationDate { get; set; }
        public string Title { get; set; }
    }
}

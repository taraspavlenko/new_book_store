﻿using System;

namespace BookStore.Services.Views.GenreViews
{
    public class GetGenreView
    {
        public string Id { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime? ModificationDate { get; set; }
        public string Title { get; set; }
    }
}

﻿using System.Collections.Generic;

namespace BookStore.Services.Views.MagazineViews
{
    public class UpdateMagazineView
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public double Price { get; set; }
        public string Url { get; set; }
        public string Description { get; set; }
        public List<string> BooksId { get; set; }
    }
}

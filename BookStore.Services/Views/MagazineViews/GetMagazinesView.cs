﻿using System;
using System.Collections.Generic;

namespace BookStore.Services.Views.MagazineViews
{
    public class GetMagazinesView
    {
        public List<MagazineGetMagazinesViewItem> Magazines { get; set; }
        public GetMagazinesView()
        {
            Magazines = new List<MagazineGetMagazinesViewItem>();
        }
    }

    public class MagazineGetMagazinesViewItem
    {
        public string Id { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime? ModificationDate { get; set; }
        public string Title { get; set; }
        public double Price { get; set; }
        public string Url { get; set; }
        public string Description { get; set; }
        public List<BookGetMagazinesViewItem> Books { get; set; } = new List<BookGetMagazinesViewItem>();
    }

    public class BookGetMagazinesViewItem
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
    }
}

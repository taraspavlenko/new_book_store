﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookStore.Services.Views.MagazineViews
{
    public class GetMagazineView
    {
        public string Id { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime? ModificationDate { get; set; }
        public string Title { get; set; }
        public double Price { get; set; }
        public string Url { get; set; }
        public string Description { get; set; }
        public List<BookGetMagazineViewItem> Books { get; set; } = new List<BookGetMagazineViewItem>();
    }

    public class BookGetMagazineViewItem
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
    }
}

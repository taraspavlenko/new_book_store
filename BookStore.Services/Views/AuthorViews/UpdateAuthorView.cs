﻿namespace BookStore.Services.Views.AuthorViews
{
    public class UpdateAuthorView
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace BookStore.Services.Views.AuthorViews
{
    public class GetAuthorsView
    {
        public List<AuthorGetAuthorsViewItem> Authors { get; set; } 

        public GetAuthorsView()
        {
            Authors = new List<AuthorGetAuthorsViewItem>();
        }
    }

    public class AuthorGetAuthorsViewItem
    {
        public string Id { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime? ModificationDate { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}

﻿using System;

namespace BookStore.Services.Views.AuthorViews
{
    public class GetAuthorView
    {
        public string Id { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime? ModificationDate { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

    }
}

﻿namespace BookStore.Services.Views.AuthorViews
{
    public class CreateAuthorView
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}

﻿using System.Collections.Generic;

namespace BookStore.Services.Views.UserViews
{
    public class GetUserView
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public List<OrderGerUserView> Orders { get; set; }
    }

    public class OrderGerUserView
    {
        public string Id { get; set; }
        public bool Confirmed { get; set; }
        public bool Payed { get; set; }
        public double TotalPrice { get; set; }
    }
}

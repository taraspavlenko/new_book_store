﻿using System.Collections.Generic;

namespace BookStore.Services.Views.UserViews
{
    public class GetUsersView
    {
        public List<UserGetUsersViewItem> Users { get; set; } = new List<UserGetUsersViewItem>();
    }

    public class UserGetUsersViewItem
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public List<OrderGerUsersView> Orders { get; set; } = new List<OrderGerUsersView>();
        public List<string> Role { get; set; } = new List<string>();
    }

    public class OrderGerUsersView
    {
        public string Id { get; set; }
        public bool Confirmed { get; set; }
        public bool Payed { get; set; }
        public double TotalPrice { get; set; }
    }
}

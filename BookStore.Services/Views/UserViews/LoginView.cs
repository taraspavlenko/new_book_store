﻿namespace BookStore.Services.Views.UserViews
{
    public class LoginView
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}

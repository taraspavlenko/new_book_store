﻿namespace BookStore.Services.Views.UserViews
{
    public class RegisterUserView
    {
        public string UserName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Password { get; set; }

        public string Content { get; set; }
    }
}

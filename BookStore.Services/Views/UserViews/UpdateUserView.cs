﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookStore.Services.Views.UserViews
{
    public class UpdateUserView
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Password { get; set; }
    }
}

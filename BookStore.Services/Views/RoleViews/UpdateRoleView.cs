﻿namespace BookStore.Services.Views.RoleViews
{
    public class UpdateRoleView
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}

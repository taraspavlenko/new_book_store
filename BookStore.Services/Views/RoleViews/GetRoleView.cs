﻿namespace BookStore.Services.Views.RoleViews
{
    public class GetRoleView
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}

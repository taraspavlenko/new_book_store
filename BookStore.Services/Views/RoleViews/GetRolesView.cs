﻿using System.Collections.Generic;

namespace BookStore.Services.Views.RoleViews
{
    public class GetRolesView
    {
        public List<RoleGetRolesViewItem> Roles { get; set; }
        public GetRolesView()
        {
            Roles = new List<RoleGetRolesViewItem>();
        }
    }

    public class RoleGetRolesViewItem
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}

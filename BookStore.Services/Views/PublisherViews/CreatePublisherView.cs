﻿namespace BookStore.Services.Views.PublisherViews
{
    public class CreatePublisherView
    {
        public string Title { get; set; }
    }
}

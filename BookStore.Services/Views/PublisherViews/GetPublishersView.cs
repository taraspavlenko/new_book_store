﻿using System;
using System.Collections.Generic;

namespace BookStore.Services.Views.PublisherViews
{
    public class GetPublishersView
    {
            public List<PublisherGetPublishersViewItem> Publishers { get; set; }

            public GetPublishersView()
            {
            Publishers = new List<PublisherGetPublishersViewItem>();
            }
    }
    public class PublisherGetPublishersViewItem
    {
        public string Id { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime? ModificationDate { get; set; }
        public string Title { get; set; }
    }
}

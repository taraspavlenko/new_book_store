﻿namespace BookStore.Services.Views.PublisherViews
{
    public class UpdatePublisherView
    {
        public string Id { get; set; }
        public string Title { get; set; }
    }
}

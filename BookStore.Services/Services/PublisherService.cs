﻿using AutoMapper;
using BookStore.DataAccess.Entities;
using BookStore.DataAccess.Repositories.Interfaces;
using BookStore.Services.Interfaces;
using BookStore.Services.Views.PublisherViews;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore.Services.Services
{
    public class PublisherService : IPublisherService
    {
        private readonly IPublisherRepository _publisherRepository;
        private readonly IMapper _mapper;

        public PublisherService(IPublisherRepository publisherRepository, IMapper mapper)
        {
            _publisherRepository = publisherRepository;
            _mapper = mapper;
        }
        public async Task CreateAsync(CreatePublisherView createPublisherView)
        {
            Publisher publisher = await GetIfExist(createPublisherView.Title);
            if (publisher != null)
            {
                if (!publisher.Deleted)
                {
                    throw new Exception("The publisher already exists");
                }
                publisher.Deleted = false;
                await _publisherRepository.UpdateAsync(publisher);
            }
            publisher = _mapper.Map<Publisher>(createPublisherView);

            await _publisherRepository.InsertAsync(publisher);
        }

        public async Task<GetPublisherView> GetByIdAsync(string id)
        {
            Publisher publisher = await _publisherRepository.GetByIdAsync(id);
            if (publisher.Deleted)
            {
                throw new Exception($"The {nameof(Publisher).ToLower()} was deleted");
            }
            var publisherView = _mapper.Map<GetPublisherView>(publisher);

            return publisherView;
        }

        public async Task<GetPublishersView> GetAllAsync()
        {
            var publishers = await _publisherRepository.GetAllAsync();
            publishers = publishers.Where(x => x.Deleted == false).ToList();

            var publisherView = new GetPublishersView();
            publisherView.Publishers = _mapper.Map<List<PublisherGetPublishersViewItem>>(publishers);

            return publisherView;
        }

        public async Task UpdateAsync(UpdatePublisherView updatePublisherView)
        {
            Publisher publisher = await _publisherRepository.GetByIdAsync(updatePublisherView.Id);
            if (publisher.Deleted)
            {
                throw new Exception($"The {nameof(Publisher).ToLower()} was deleted");
            }
            publisher.Title = updatePublisherView.Title;
            publisher.ModificationDate = DateTime.UtcNow;

            await _publisherRepository.UpdateAsync(publisher);
        }
        public async Task DeleteAsync(string id)
        {
            Publisher publisher = await _publisherRepository.GetByIdAsync(id);
            if (publisher == null)
            {
                throw new Exception("Not found");
            }
            publisher.Deleted = true;
            await _publisherRepository.UpdateAsync(publisher);
        }
        public async Task RestoreAsync(string id)
        {
            Publisher publisher = await _publisherRepository.GetByIdAsync(id);
            if (publisher == null)
            {
                throw new Exception("Not found");
            }
            publisher.Deleted = false;
            await _publisherRepository.UpdateAsync(publisher);
        }
        private async Task<Publisher> GetIfExist(string title)
        {
            var publishersList = await _publisherRepository.GetAllAsync();
            return publishersList.Where(x => x.Title == title).FirstOrDefault();
        }
    }
}

﻿using AutoMapper;
using BookStore.DataAccess.Entities;
using BookStore.DataAccess.Repositories.Interfaces;
using BookStore.Services.Interfaces;
using BookStore.Services.Views.BookViews;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace BookStore.Services.Services
{
    public class BookService : IBookService
    {
        private readonly IBookRepository _bookRepository;

        private readonly IBookAuthorRepository _bookAuthorRepository;
        private readonly IBookPublisherRepository _bookPublisherRepository;
        private readonly IBookGenreRepository _bookGenreRepository;
        private readonly IMapper _mapper;

        public BookService(IBookRepository bookRepository,
                           IBookAuthorRepository bookAuthorRepository,
                           IBookPublisherRepository bookPublisherRepository,
                           IBookGenreRepository bookGenreRepository,
                           IMapper mapper)
        {
            _bookRepository = bookRepository;
            _bookAuthorRepository = bookAuthorRepository;
            _bookPublisherRepository = bookPublisherRepository;
            _bookGenreRepository = bookGenreRepository;
            _mapper = mapper;
        }

        public async Task<GetBooksView> GetAllAsync()
        {
            var books = await _bookRepository.GetAllAsync();
            books = books.Where(x => x.Deleted == false).ToList();
            foreach (var book in books)
            {
                book.BookAuthors = await _bookAuthorRepository.GetByBookIdAsync(book.Id);
                book.BookPublishers = await _bookPublisherRepository.GetByBookIdAsync(book.Id);
                book.BookGenres = await _bookGenreRepository.GetByBookIdAsync(book.Id);
            }
            var booksList = new GetBooksView();
            booksList.Books =_mapper.Map<List<BookGetBooksViewItem>>(books);

            return booksList;
        }
        public async Task<GetBookView> GetByIdAsync(string bookId)
        {
            Book book = await _bookRepository.GetByIdAsync(bookId);
            if (book.Deleted)
            {
                throw new Exception($"The {nameof(Book).ToLower()} was deleted");
            }
            book.BookAuthors = await _bookAuthorRepository.GetByBookIdAsync(bookId);
            book.BookPublishers = await _bookPublisherRepository.GetByBookIdAsync(bookId);
            book.BookGenres = await _bookGenreRepository.GetByBookIdAsync(bookId);

            var result = _mapper.Map<GetBookView>(book);

            return result;

        }
        public async Task CreateAsync(CreateBookView createBookView)
        {
            Book book = await GetIfExistAsync(createBookView.Title, createBookView.Url);
            if (book != null)
            {
                if (!book.Deleted)
                {
                    throw new Exception("The book already exists");
                }
                book.Deleted = false;
                await _bookRepository.UpdateAsync(book);
            }
            else
            {
                book = _mapper.Map<Book>(createBookView);

                var bookAuthorsList = new List<BookAuthor>();
                bookAuthorsList
                    .AddRange(createBookView.AuthorsId
                    .Select(x => new BookAuthor { BookId = book.Id, AuthorId = x }));

                var bookPublishersList = new List<BookPublisher>();
                bookPublishersList
                    .AddRange(createBookView.PublishersId
                    .Select(x => new BookPublisher { BookId = book.Id, PublisherId = x }));

                var bookGenresList = new List<BookGenre>();
                bookGenresList
                     .AddRange(createBookView.GenresId
                    .Select(x => new BookGenre { BookId = book.Id, GenreId = x }));

                await _bookRepository.InsertAsync(book);

                await _bookAuthorRepository.InsertRangeAsync(bookAuthorsList);
                await _bookPublisherRepository.InsertRangeAsync(bookPublishersList);
                await _bookGenreRepository.InsertRangeAsync(bookGenresList);
            }
            
        }
        public async Task UpdateAsync(UpdateBookView updateBookView)
        {
            Book book = await _bookRepository.GetByIdAsync(updateBookView.Id);
            if (book.Deleted)
            {
                throw new Exception($"The {nameof(Book).ToLower()} was deleted");
            }
            book.ModificationDate = DateTime.UtcNow;
            book.Price = updateBookView.Price;
            book.Title = updateBookView.Title;
            book.Url = updateBookView.Url;
            book.Description = updateBookView.Description;

            var bookAuthorsList = new List<BookAuthor>();
            bookAuthorsList
                .AddRange(updateBookView.AuthorsId
                .Select(x => new BookAuthor { BookId = book.Id, AuthorId = x }));

            var bookPublishersList = new List<BookPublisher>();
            bookPublishersList
                .AddRange(updateBookView.PublishersId
                .Select(x => new BookPublisher { BookId = book.Id, PublisherId = x }));

            var bookGenresList = new List<BookGenre>();
            bookGenresList
                 .AddRange(updateBookView.GenresId
                .Select(x => new BookGenre { BookId = book.Id, GenreId = x }));


            var dbBookAuthorsList = await _bookAuthorRepository.GetByBookIdAsync(book.Id);
            var dbBookPublishersList = await _bookPublisherRepository.GetByBookIdAsync(book.Id);
            var dbBookGenresList = await _bookGenreRepository.GetByBookIdAsync(book.Id);

            await _bookRepository.UpdateAsync(book);

            _bookAuthorRepository.DeleteRange(dbBookAuthorsList);
            _bookPublisherRepository.DeleteRange(dbBookPublishersList);
            _bookGenreRepository.DeleteRange(dbBookGenresList);

            await _bookAuthorRepository.InsertRangeAsync(bookAuthorsList);
            await _bookPublisherRepository.InsertRangeAsync(bookPublishersList);
            await _bookGenreRepository.InsertRangeAsync(bookGenresList);
        }
        public async Task DeleteAsync(string id)
        {
            Book book = await _bookRepository.GetByIdAsync(id);
            book.Deleted = true;
            await _bookRepository.UpdateAsync(book);

            var dbBookAuthorsList = await _bookAuthorRepository.GetByBookIdAsync(book.Id);
            var dbBookPublishersList = await _bookPublisherRepository.GetByBookIdAsync(book.Id);
            var dbBookGenresList = await _bookGenreRepository.GetByBookIdAsync(book.Id);

            _bookAuthorRepository.DeleteRange(dbBookAuthorsList);
            _bookPublisherRepository.DeleteRange(dbBookPublishersList);
            _bookGenreRepository.DeleteRange(dbBookGenresList);
        }
        public async Task RestoreAsync(string id)
        {
            Book book = await _bookRepository.GetByIdAsync(id);
            book.Deleted = false;
            await _bookRepository.UpdateAsync(book);
        }
        private async Task<Book> GetIfExistAsync(string title, string url)
        {
            var booksList = await _bookRepository.GetAllAsync();
            return booksList.Where(x => x.Title == title && x.Url == url).FirstOrDefault();
        }

    }
}

﻿using AutoMapper;
using BookStore.DataAccess.Entities;
using BookStore.DataAccess.Repositories.Interfaces;
using BookStore.Services.Interfaces;
using BookStore.Services.Views.GenreViews;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore.Services.Services
{
    public class GenreService : IGenreService
    {
        private readonly IGenreRepository _genreRepository;
        private readonly IMapper _mapper;

        public GenreService(IGenreRepository genreRepository, IMapper mapper)
        {
            _genreRepository = genreRepository;
            _mapper = mapper;
        }

        public async Task CreateAsync(CreateGenreView createGenreView)
        {
            Genre genre = await GetIfExist(createGenreView.Title);
            if (genre != null)
            {
                if (!genre.Deleted)
                {
                    throw new Exception("The author already exists");
                }
                genre.Deleted = false;
                await _genreRepository.UpdateAsync(genre);
            }
            genre = _mapper.Map<Genre>(createGenreView);

            await _genreRepository.InsertAsync(genre);
        }

        public async Task<GetGenreView> GetByIdAsync(string id)
        {
            Genre genre = await _genreRepository.GetByIdAsync(id);
            if (genre.Deleted)
            {
                throw new Exception($"The {nameof(Genre).ToLower()} was deleted");
            }
            var genreView = _mapper.Map<GetGenreView>(genre);

            return genreView;
        }

        public async Task<GetGenresView> GetAllAsync()
        {
            var genres = await _genreRepository.GetAllAsync();
            genres = genres.Where(x => x.Deleted == false).ToList();

            var genresView = new GetGenresView();
            genresView.Genres = _mapper.Map<List<GenreGetGenresViewItem>>(genres);

            return genresView;
        }

        public async Task UpdateAsync(UpdateGenreView genreView)
        {
            Genre genre = await _genreRepository.GetByIdAsync(genreView.Id);
            if (genre.Deleted)
            {
                throw new Exception($"The {nameof(Genre).ToLower()} was deleted");
            }
            genre.Title = genreView.Title;
            genre.ModificationDate = DateTime.UtcNow;

            await _genreRepository.UpdateAsync(genre);
        }
        public async Task DeleteAsync(string id)
        {
            Genre genre = await _genreRepository.GetByIdAsync(id);
            if (genre == null)
            {
                throw new Exception("Not found");
            }
            genre.Deleted = true;
            await _genreRepository.UpdateAsync(genre);
        }
        public async Task RestoreAsync(string id)
        {
            Genre genre = await _genreRepository.GetByIdAsync(id);
            if (genre == null)
            {
                throw new Exception("Not found");
            }
            genre.Deleted = false;
            await _genreRepository.UpdateAsync(genre);
        }
        private async Task<Genre> GetIfExist(string title)
        {
            var genresList = await _genreRepository.GetAllAsync();
            return genresList.Where(x => x.Title == title).FirstOrDefault();
        }
    }
}

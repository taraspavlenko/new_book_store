﻿using AutoMapper;
using BookStore.DataAccess.Entities;
using BookStore.DataAccess.Repositories.Interfaces;
using BookStore.Services.Interfaces;
using BookStore.Services.Views.AuthorViews;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore.Services.Services
{
    public class AuthorService : IAuthorService
    {
        private readonly IAuthorRepository _authorRepository;
        private readonly IMapper _mapper;

        public AuthorService(IAuthorRepository authorRepository, IMapper mapper)
        {
            _authorRepository = authorRepository;
            _mapper = mapper;
        }
        public async Task CreateAsync(CreateAuthorView createAuthorView)
        {
            Author author = await GetIfExist(createAuthorView.FirstName, createAuthorView.LastName);
            if (author != null)
            {
                if (!author.Deleted)
                {
                    throw new Exception("The author already exists");
                }
                author.Deleted = false;
                await _authorRepository.UpdateAsync(author);
            }
            else
            {
                author = _mapper.Map<Author>(createAuthorView);
                await _authorRepository.InsertAsync(author);
            }
           
        }
        public async Task<GetAuthorView> GetByIdAsync(string id)
        {
            Author author = await _authorRepository.GetByIdAsync(id);
            if (author.Deleted)
            {
                throw new Exception($"The {nameof(Author).ToLower()} was deleted");
            }
            var authorsView = _mapper.Map<GetAuthorView>(author);

            return authorsView;
        }

        public async Task<GetAuthorsView> GetAllAsync()
        {
            var authors = await _authorRepository.GetAllAsync();
            authors = authors.Where(x => x.Deleted == false).ToList();

            var authorsView = new GetAuthorsView();
            authorsView.Authors = _mapper.Map<List<AuthorGetAuthorsViewItem>>(authors);
            return authorsView;
        }

        public async Task UpdateAsync(UpdateAuthorView updateAuthorView)
        {
            Author author = await _authorRepository.GetByIdAsync(updateAuthorView.Id);
            if (author.Deleted)
            {
                throw new Exception($"The {nameof(Author).ToLower()} was deleted");
            }
            author.FirstName = updateAuthorView.FirstName;
            author.LastName = updateAuthorView.LastName;
            author.ModificationDate = DateTime.UtcNow;

            await _authorRepository.UpdateAsync(author);
        }
        public async Task DeleteAsync(string id)
        {
            Author author = await _authorRepository.GetByIdAsync(id);
            if (author == null)
            {
                throw new Exception("Not found");
            }
            author.Deleted = true;
            await _authorRepository.UpdateAsync(author);
        }

        public async Task RestoreAsync(string id)
        {
            Author author = await _authorRepository.GetByIdAsync(id);
            if (author == null)
            {
                throw new Exception("Not found");
            }
            author.Deleted = false;
            await _authorRepository.UpdateAsync(author);
        }
        private async Task<Author> GetIfExist(string firstName, string lastName)
        {
            var authorsList = await _authorRepository.GetAllAsync();
            return authorsList.Where(x => x.FirstName == firstName && x.LastName == lastName).FirstOrDefault();
        }
    }
}

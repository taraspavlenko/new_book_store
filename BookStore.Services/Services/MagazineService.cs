﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BookStore.DataAccess.Entities;
using BookStore.DataAccess.Repositories.Interfaces;
using BookStore.Services.Interfaces;
using BookStore.Services.Views.MagazineViews;

namespace BookStore.Services.Services
{
    public class MagazineService : IMagazineService
    {
        private readonly IMagazineRepository _magazineRepository;
        private readonly IMagazineBookRepository _magazineBookRepository;
        private readonly IMapper _mapper;

        public MagazineService(IMagazineRepository magazineRepository,
                               IMagazineBookRepository magazineBookRepository,
                               IMapper mapper)
        {
            _magazineRepository = magazineRepository;
            _magazineBookRepository = magazineBookRepository;
            _mapper = mapper;
        }

        public async Task<GetMagazinesView> GetAllAsync()
        {
            var magazines = await _magazineRepository.GetAllAsync();
            magazines = magazines.Where(x => x.Deleted == false).ToList();
            foreach (var magazine in magazines)
            {
                magazine.MagazineBooks = await _magazineBookRepository.GetByMagazineIdAsync(magazine.Id);
            }
            var magazinesList = new GetMagazinesView();
            magazinesList.Magazines = _mapper.Map<List<MagazineGetMagazinesViewItem>>(magazines);

            return magazinesList;
        }
        public async Task<GetMagazineView> GetByIdAsync(string magazineId)
        {
            Magazine magazine = await _magazineRepository.GetByIdAsync(magazineId);
            if (magazine.Deleted)
            {
                throw new Exception($"The {nameof(Magazine).ToLower()} was deleted");
            }
            magazine.MagazineBooks = await _magazineBookRepository.GetByMagazineIdAsync(magazineId);

            var result = _mapper.Map<GetMagazineView>(magazine);
            return result;
        }
        public async Task CreateAsync(CreateMagazineView createMagazineView)
        {
            Magazine magazine = await GetIfExist(createMagazineView.Title, createMagazineView.Url);
            if (magazine != null)
            {
                if (!magazine.Deleted)
                {
                    throw new Exception("The magazine already exists");
                }
                magazine.Deleted = false;
                await _magazineRepository.UpdateAsync(magazine);
            }
            else
            {
                magazine = _mapper.Map<Magazine>(createMagazineView);
                var magazineBookList = CreateMagazineBookList(createMagazineView.BooksId, magazine.Id);

                await _magazineRepository.InsertAsync(magazine);

                await _magazineBookRepository.InsertRangeAsync(magazineBookList);
            }

        }
        public async Task UpdateAsync(UpdateMagazineView updateMagazineView)
        {
            Magazine magazine = await _magazineRepository.GetByIdAsync(updateMagazineView.Id);
            if (magazine.Deleted)
            {
                throw new Exception($"The {nameof(Magazine).ToLower()} was deleted");
            }
            magazine.ModificationDate = DateTime.UtcNow;
            magazine.Price = updateMagazineView.Price;
            magazine.Title = updateMagazineView.Title;
            magazine.Url = updateMagazineView.Url;
            magazine.Description = updateMagazineView.Description;

            var magazineBookList = CreateMagazineBookList(updateMagazineView.BooksId, magazine.Id);

            var dbMagazineBookList = await _magazineBookRepository.GetByMagazineIdAsync(magazine.Id);

            await _magazineRepository.UpdateAsync(magazine);

            _magazineBookRepository.DeleteRange(dbMagazineBookList);

            await _magazineBookRepository.InsertRangeAsync(magazineBookList);
        }
        public async Task DeleteAsync(string id)
        {
            Magazine magazine = await _magazineRepository.GetByIdAsync(id);
            magazine.Deleted = true;
            await _magazineRepository.UpdateAsync(magazine);

            var dbMagazineBookList = await _magazineBookRepository.GetByMagazineIdAsync(magazine.Id);
            _magazineBookRepository.DeleteRange(dbMagazineBookList);
        }
        public async Task Restore(string id)
        {
            Magazine magazine = await _magazineRepository.GetByIdAsync(id);
            magazine.Deleted = false;
            await _magazineRepository.UpdateAsync(magazine);

            var dbMagazineBookList = await _magazineBookRepository.GetByMagazineIdAsync(magazine.Id);
            _magazineBookRepository.DeleteRange(dbMagazineBookList);
        }

        private async Task<Magazine> GetIfExist(string title, string url)
        {
            var magazineList = await _magazineRepository.GetAllAsync();
            return magazineList.Where(x => x.Title == title && x.Url == url).FirstOrDefault();
        }
      
        private List<MagazineBook> CreateMagazineBookList(List<string> booksIdList, string magazineId)
        {
            var magazineBooksList = new List<MagazineBook>();
            foreach (var bookId in booksIdList)
            {
                magazineBooksList.Add(new MagazineBook { MagazineId = magazineId, BookId = bookId });
            }
            return magazineBooksList;
        }
    }
}

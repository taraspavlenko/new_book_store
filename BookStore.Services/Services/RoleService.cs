﻿using AutoMapper;
using BookStore.DataAccess.Entities;
using BookStore.Services.Interfaces;
using BookStore.Services.Views.RoleViews;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore.Services.Services
{
    public class RoleService : IRoleService
    {
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly IMapper _mapper;

        public RoleService(RoleManager<IdentityRole> roleManager, IMapper mapper)
        {
            _roleManager = roleManager;
            _mapper = mapper;
        }

        public async Task CreateAsync(CreateRoleView createRoleView)
        {
            var role = new IdentityRole(createRoleView.Name);
            await _roleManager.CreateAsync(role);
        }

        public Task DeleteAsync(string roleId)
        {
            throw new NotImplementedException();
        }

        public async Task<GetRolesView> GetAllAsync()
        {
            var roles = await _roleManager.Roles.ToListAsync();
            var rolesList = new GetRolesView();
            rolesList.Roles = _mapper.Map<List<IdentityRole>, List<RoleGetRolesViewItem>>(roles);

            return rolesList;
        }

        public async Task<GetRoleView> GetByIdAsync(string roleId)
        {
            var role = await _roleManager.Roles.Where(x => x.Id == roleId).FirstOrDefaultAsync();
            var result = _mapper.Map<IdentityRole, GetRoleView>(role);
            return result;
        }

        public Task RestoreAsync(string roleId)
        {
            throw new NotImplementedException();
        }

        public Task UpdateAsync(UpdateRoleView updateRoleView)
        {
            throw new NotImplementedException();
        }
    }
}

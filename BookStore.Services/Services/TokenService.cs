﻿using BookStore.DataAccess.Entities;
using BookStore.Services.Interfaces;
using BookStore.Services.Views;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace BookStore.Services.Services
{
    public class TokenService : ITokenService
    {
        private readonly UserManager<User> _userManager;

        public TokenService(UserManager<User> userManager)
        {
            _userManager = userManager;
        }
        public async Task<string> GenerateAccessJwtTokenAsync(User user)
        {
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Email, user.Email),
                new Claim(JwtRegisteredClaimNames.UniqueName, user.UserName),
                new Claim(JwtRegisteredClaimNames.NameId, user.Id)
            };
            claims.AddRange(await _userManager.GetClaimsAsync(user));

            var userRoles = (await _userManager.GetRolesAsync(user)).ToList();

            claims.AddRange(userRoles.Select(x => new Claim(ClaimsIdentity.DefaultRoleClaimType, x)).ToList());

            var key = AuthOptions.GetSymmetricSecurityKey();
            var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var expires = DateTime.UtcNow.AddMinutes(AuthOptions.AccessLifeTime);

            var token = new JwtSecurityToken(
                issuer: AuthOptions.Issuer,
                audience: AuthOptions.Audience,
                claims: claims,
                expires: expires,
                signingCredentials: credentials);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        public async Task<string> GenerateEmailConfirmationTokenAsync(User user)
        {
            return await _userManager.GenerateEmailConfirmationTokenAsync(user);
        }

        public async Task<string> GenerateRefreshJwtTokenAsync(User user)
        {
            var refreshToken = await _userManager.CreateSecurityTokenAsync(user);
            return Convert.ToBase64String(refreshToken);
        }

        public async Task<TokenView> RefreshAsync(TokenView tokenView)
        {
            var principal = GetPrincipalFromExpiredToken(tokenView.AccessToken);
            var user = await _userManager.GetUserAsync(principal);
            var savedRefreshToken = await _userManager.GetAuthenticationTokenAsync(user, JwtBearerDefaults.AuthenticationScheme, TokenNames.RefreshToken);
            if (savedRefreshToken != tokenView.RefreshToken)
            {
                throw new SecurityTokenException("Invalid refresh token");
            }

            var newAccessJwtToken = await GenerateAccessJwtTokenAsync(user);
            var newRefreshJwtToken = await GenerateRefreshJwtTokenAsync(user);

            await _userManager.RemoveAuthenticationTokenAsync(user, JwtBearerDefaults.AuthenticationScheme, TokenNames.RefreshToken);
            await SetAuthenticationTokenAsync(user, JwtBearerDefaults.AuthenticationScheme, TokenNames.RefreshToken, newRefreshJwtToken);

            return new TokenView
            {
                AccessToken = newAccessJwtToken,
                RefreshToken = newRefreshJwtToken
            };
        }

        public async Task SetAuthenticationTokenAsync(User user, string authenticationScheme, string tokenName, string tokenValue)
        {
            await _userManager.SetAuthenticationTokenAsync(user, authenticationScheme, tokenName, tokenValue);
        }
        private ClaimsPrincipal GetPrincipalFromExpiredToken(string token)
        {
            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuer = true,
                ValidIssuer = AuthOptions.Issuer,
                ValidateAudience = true,
                ValidAudience = AuthOptions.Audience,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = AuthOptions.GetSymmetricSecurityKey(),
                ValidateLifetime = false 
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            SecurityToken securityToken;
            var principal = tokenHandler.ValidateToken(token, tokenValidationParameters, out securityToken);
            var jwtSecurityToken = securityToken as JwtSecurityToken;
            if (jwtSecurityToken == null || !jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256, StringComparison.InvariantCultureIgnoreCase))
                throw new SecurityTokenException("Invalid token");

            return principal;
        }
    }
}

﻿using System.Threading.Tasks;
using BookStore.DataAccess.Entities;
using BookStore.Services.Views;

namespace BookStore.Services.Interfaces
{
    public interface ITokenService
    {
        Task<string> GenerateAccessJwtTokenAsync(User user);
        Task<string> GenerateRefreshJwtTokenAsync(User user);
        Task<TokenView> RefreshAsync(TokenView tokenView);
        Task<string> GenerateEmailConfirmationTokenAsync(User user);
        Task SetAuthenticationTokenAsync(User user, string authenticationScheme, string tokenName, string tokenValue);
    }
}

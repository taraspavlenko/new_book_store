﻿using BookStore.Services.Views.PublisherViews;
using System.Threading.Tasks;

namespace BookStore.Services.Interfaces
{
    public interface IPublisherService
    {
        Task<GetPublishersView> GetAllAsync();
        Task<GetPublisherView> GetByIdAsync(string id);
        Task CreateAsync(CreatePublisherView publisherView);
        Task UpdateAsync(UpdatePublisherView publisherView);
        Task DeleteAsync(string id);
        Task RestoreAsync(string id);
    }
}

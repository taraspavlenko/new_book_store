﻿using BookStore.Services.Views.BookViews;
using System.Threading.Tasks;

namespace BookStore.Services.Interfaces
{
    public interface IBookService
    {
        Task<GetBooksView> GetAllAsync();
        Task<GetBookView> GetByIdAsync(string id);
        Task CreateAsync(CreateBookView createBookView);
        Task UpdateAsync(UpdateBookView modifyBookView);
        Task DeleteAsync(string id);
        Task RestoreAsync(string id);
    }
}

﻿using BookStore.Services.Views.MagazineViews;
using System.Threading.Tasks;

namespace BookStore.Services.Interfaces
{
    public interface IMagazineService
    {
        Task<GetMagazinesView> GetAllAsync();
        Task<GetMagazineView> GetByIdAsync(string id);
        Task CreateAsync(CreateMagazineView createMagazineView);
        Task UpdateAsync(UpdateMagazineView updateMagazineView);
        Task DeleteAsync(string id);
        Task Restore(string id);
    }
}

﻿using BookStore.Services.Views.GenreViews;
using System.Threading.Tasks;

namespace BookStore.Services.Interfaces
{
    public interface IGenreService
    {
        Task<GetGenresView> GetAllAsync();
        Task<GetGenreView> GetByIdAsync(string id);
        Task CreateAsync(CreateGenreView genreView);
        Task UpdateAsync(UpdateGenreView genreView);
        Task DeleteAsync(string id);
        Task RestoreAsync(string id);
    }
}

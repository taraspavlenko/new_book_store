﻿using BookStore.Services.Views.RoleViews;
using System.Threading.Tasks;

namespace BookStore.Services.Interfaces
{
    public interface IRoleService
    {
        Task<GetRolesView> GetAllAsync();
        Task<GetRoleView> GetByIdAsync(string roleId);
        Task CreateAsync(CreateRoleView createRoleView);
        Task UpdateAsync(UpdateRoleView updateRoleView);
        Task DeleteAsync(string roleId);
        Task RestoreAsync(string roleId);
    }
}

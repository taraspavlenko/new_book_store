﻿using System.Threading.Tasks;
using BookStore.DataAccess.Entities;
using BookStore.Services.Views;
using BookStore.Services.Views.UserViews;
using Microsoft.AspNetCore.Identity;

namespace BookStore.Services.Interfaces
{
    public interface IUserService
    {
        Task<GetUsersView> GetAllAsync();
        Task<GetUserView> GetByIdAsync(string id);
        Task UpdateAsync(UpdateUserView updateUserView);
        Task DeleteAsync(string id);
        Task RestoreAsync(string id);
        Task<(string emailToken, string userId)> RegisterAsync(RegisterUserView registerUserView);
        Task<TokenView> LoginAsync(LoginView loginView);
        Task<IdentityResult> ConfirmEmailAsyncAsync(User user, string token);
        Task<User> FindByIdAsync(string userId);
    }
}

﻿using BookStore.Services.Views.AuthorViews;
using System.Threading.Tasks;

namespace BookStore.Services.Interfaces
{
    public interface IAuthorService
    {
        Task CreateAsync(CreateAuthorView createAuthorView);
        Task<GetAuthorsView> GetAllAsync();
        Task<GetAuthorView> GetByIdAsync(string id);
        Task UpdateAsync(UpdateAuthorView authorView);
        Task DeleteAsync(string id);
        Task RestoreAsync(string id);
    }
}

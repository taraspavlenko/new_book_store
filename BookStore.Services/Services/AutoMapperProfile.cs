﻿using AutoMapper;
using BookStore.DataAccess.Entities;
using BookStore.Services.Views.AuthorViews;
using BookStore.Services.Views.BookViews;
using BookStore.Services.Views.GenreViews;
using BookStore.Services.Views.MagazineViews;
using BookStore.Services.Views.PublisherViews;
using System;

namespace BookStore.Services.Services
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Book, GetBookView>()
                 .ForMember(pts => pts.Authors, opt => opt.MapFrom(ps => ps.BookAuthors))
                 .ForMember(pts => pts.Publishers, opt => opt.MapFrom(ps => ps.BookPublishers))
                 .ForMember(pts => pts.Genres, opt => opt.MapFrom(ps => ps.BookGenres));
            CreateMap<BookAuthor, AuthorGetBookViewItem>()
                    .ForPath(pts => pts.Id, opt => opt.MapFrom(ps => ps.Author.Id))
                    .ForPath(pts => pts.FirstName, opt => opt.MapFrom(ps => ps.Author.FirstName))
                    .ForPath(pts => pts.LastName, opt => opt.MapFrom(ps => ps.Author.LastName));
            CreateMap<BookGenre, GenreGetBookViewItem>()
                .ForPath(pts => pts.Id, opt => opt.MapFrom(ps => ps.Genre.Id))
                .ForPath(pts => pts.Title, opt => opt.MapFrom(ps => ps.Genre.Title));
            CreateMap<BookPublisher, PublisherGetBookViewItem>()
                .ForPath(pts => pts.Id, opt => opt.MapFrom(ps => ps.Publisher.Id))
                .ForPath(pts => pts.Title, opt => opt.MapFrom(ps => ps.Publisher.Title));

            CreateMap<Book, BookGetBooksViewItem>()
                .ForMember(pts => pts.Authors, opt => opt.MapFrom(ps => ps.BookAuthors))
                .ForMember(pts => pts.Publishers, opt => opt.MapFrom(ps => ps.BookPublishers))
                .ForMember(pts => pts.Genres, opt => opt.MapFrom(ps => ps.BookGenres));
            CreateMap<BookAuthor, AuthorGetBooksViewItem>()
                   .ForPath(pts => pts.Id, opt => opt.MapFrom(ps => ps.Author.Id))
                   .ForPath(pts => pts.FirstName, opt => opt.MapFrom(ps => ps.Author.FirstName))
                   .ForPath(pts => pts.LastName, opt => opt.MapFrom(ps => ps.Author.LastName));
            CreateMap<BookGenre, GenreGetBooksViewItem>()
                .ForPath(pts => pts.Id, opt => opt.MapFrom(ps => ps.Genre.Id))
                .ForPath(pts => pts.Title, opt => opt.MapFrom(ps => ps.Genre.Title));
            CreateMap<BookPublisher, PublisherGetBooksViewItem>()
                .ForPath(pts => pts.Id, opt => opt.MapFrom(ps => ps.Publisher.Id))
                .ForPath(pts => pts.Title, opt => opt.MapFrom(ps => ps.Publisher.Title));
            
            CreateMap<Book, CreateBookView>().ReverseMap()
                .ForMember(dest => dest.Title, src => src.MapFrom(x => x.Title))
                .ForMember(dest => dest.Price, src => src.MapFrom(x => x.Price))
                .ForMember(dest => dest.Url, src => src.MapFrom(x => x.Url))
                .ForMember(dest => dest.Description, src => src.MapFrom(x => x.Description));

            CreateMap<CreateAuthorView, Author>()
                .ForMember(dest => dest.Id, src => src.Ignore())
                .ForMember(dest => dest.CreationDate, src => src.Ignore());
            CreateMap<Author, GetAuthorView>();
            CreateMap<Author, AuthorGetAuthorsViewItem>();

            CreateMap<CreateGenreView, Genre>()
              .ForMember(dest => dest.Id, src => src.Ignore())
               .ForMember(dest => dest.CreationDate, src => src.Ignore());
            CreateMap<Genre, GetGenreView>();
            CreateMap<Genre, GenreGetGenresViewItem>();

            CreateMap<CreatePublisherView, Publisher>()
              .ForMember(dest => dest.Id, src => src.Ignore())
              .ForMember(dest => dest.CreationDate, src => src.Ignore());
            CreateMap<Publisher, GetPublisherView>();
            CreateMap<Publisher, PublisherGetPublishersViewItem>();

            CreateMap<Magazine, CreateMagazineView>().ReverseMap()
                .ForMember(dest => dest.Title, src => src.MapFrom(x => x.Title))
                .ForMember(dest => dest.Price, src => src.MapFrom(x => x.Price))
                .ForMember(dest => dest.Url, src => src.MapFrom(x => x.Url))
                .ForMember(dest => dest.Description, src => src.MapFrom(x => x.Description));
            CreateMap<Magazine, GetMagazineView>()
                 .ForMember(pts => pts.Books, opt => opt.MapFrom(ps => ps.MagazineBooks));
            CreateMap<MagazineBook, BookGetMagazineViewItem>()
                .ForMember(pts => pts.Id, opt => opt.MapFrom(ps => ps.Book.Id))
                .ForMember(pts => pts.Title, opt => opt.MapFrom(ps => ps.Book.Title))
                .ForMember(pts => pts.Description, opt => opt.MapFrom(ps => ps.Book.Description));
            CreateMap<Magazine, MagazineGetMagazinesViewItem>()
               .ForMember(pts => pts.Books, opt => opt.MapFrom(ps => ps.MagazineBooks));
            CreateMap<MagazineBook, BookGetMagazinesViewItem>()
                   .ForPath(pts => pts.Id, opt => opt.MapFrom(ps => ps.Book.Id))
                   .ForPath(pts => pts.Title, opt => opt.MapFrom(ps => ps.Book.Title))
                   .ForPath(pts => pts.Description, opt => opt.MapFrom(ps => ps.Book.Description));
        }
    }
}

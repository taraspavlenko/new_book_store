﻿using AutoMapper;
using BookStore.DataAccess.Entities;
using BookStore.DataAccess.Repositories.Interfaces;
using BookStore.Services.Interfaces;
using BookStore.Services.Views;
using BookStore.Services.Views.UserViews;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore.Services.Services
{
    public class UserService : IUserService
    {
        private readonly UserManager<User> _userManager;
        private readonly IOrderRepository _orderRepository;
        private readonly ITokenService _tokenService;
        private readonly IMapper _mapper;

        public UserService(UserManager<User> userManager,
                           IOrderRepository orderRepository,
                           ITokenService tokenService,
                           IMapper mapper)
        {
            _userManager = userManager;
            _orderRepository = orderRepository;
            _tokenService = tokenService;
            _mapper = mapper;
        }

        public async Task<GetUsersView> GetAllAsync()
        {
            var users = _userManager.Users.ToList();
            
            users = users.Where(x => x.Deleted == false).ToList();
            foreach (var user in users)
            {
                user.Orders = await GetOrdersList(user.Id);
                user.Roles = await _userManager.GetRolesAsync(user);
            }
            var usersList = new GetUsersView();
            usersList.Users = _mapper.Map<List<UserGetUsersViewItem>>(users);

            return usersList;
        }

        public async Task<GetUserView> GetByIdAsync(string userId)
        {
            User user = _userManager.Users.FirstOrDefault(x => x.Id == userId);
            if (user.Deleted)
            {
                throw new HttpStatusCodeException(StatusCodes.Status400BadRequest, $"The user was deleted");
            }
            user.Orders = await GetOrdersList(userId);
            user.Roles = await _userManager.GetRolesAsync(user);

            var result = _mapper.Map<GetUserView>(user);

            return result;
        }

        public async Task<(string emailToken, string userId)> RegisterAsync(RegisterUserView registerUserView)
        {
            User user = await _userManager.FindByEmailAsync(registerUserView.Email);
            if (user != null)
            {
                if (user.Deleted)
                {
                    throw new HttpStatusCodeException(StatusCodes.Status400BadRequest, "The user was deleted");
                }
                else
                {
                    throw new HttpStatusCodeException(StatusCodes.Status400BadRequest, "The user already exists");
                }
            }
            user = new User
            {
                UserName = registerUserView.UserName,
                Email = registerUserView.Email
            };
            IdentityResult result = await _userManager.CreateAsync(user, registerUserView.Password);
            if (!result.Succeeded)
            {
                var errors = result.Errors.ToList();
                throw new HttpStatusCodeException(StatusCodes.Status400BadRequest, errors);
            }
            await _userManager.AddToRoleAsync(user, Role.User);

            string emailToken = await _tokenService.GenerateEmailConfirmationTokenAsync(user);

            return (emailToken, user.Id);
        }

        public async Task<TokenView> LoginAsync(LoginView loginView)
        {
            User user = await _userManager.FindByNameAsync(loginView.UserName);
            if (user == null)
            {
                throw new HttpStatusCodeException(StatusCodes.Status400BadRequest, "The user is not found");
            }
            if (await _userManager.GetAccessFailedCountAsync(user) > 3)
            {
                throw new HttpStatusCodeException(StatusCodes.Status400BadRequest, "The user is locked");
            }
            bool flag = await _userManager.CheckPasswordAsync(user, loginView.Password);
            if (!flag)
            {
                await _userManager.AccessFailedAsync(user);
                throw new HttpStatusCodeException(StatusCodes.Status400BadRequest, "Invalid credentials");
            }
            await _userManager.ResetAccessFailedCountAsync(user);

            string accessToken = await _tokenService.GenerateAccessJwtTokenAsync(user);
            string refreshToken = await _tokenService.GenerateRefreshJwtTokenAsync(user);

            await _tokenService.SetAuthenticationTokenAsync(user, JwtBearerDefaults.AuthenticationScheme, TokenNames.RefreshToken, refreshToken);

            return new TokenView
            {
                AccessToken = accessToken,
                RefreshToken = refreshToken
            };
        }

        public Task Logout(string accessToken)
        {
            throw new NotImplementedException();
        }

        public async Task<IdentityResult> ConfirmEmailAsyncAsync(User user, string token)
        {
            return await _userManager.ConfirmEmailAsync(user, token);
        }

        public async Task<User> FindByIdAsync(string userId)
        {
            return await _userManager.FindByIdAsync(userId);
        }
        private async Task<List<Order>> GetOrdersList(string userId)
        {
            var dbList = await _orderRepository.GetAllAsync();
            var userOrdersList = dbList.Where(i => i.UserId == userId).ToList();

            return userOrdersList;
        }

        public Task UpdateAsync(UpdateUserView updateUserView)
        {
            throw new System.NotImplementedException();
        }
        public Task DeleteAsync(string id)
        {
            throw new System.NotImplementedException();
        }
        public Task RestoreAsync(string id)
        {
            throw new System.NotImplementedException();
        }
    }
}
